// product_Format for stockIn
let product_Format={
    case:"1",
    target_warehouse:"5b1a8072df2d5d4ee2ee72c6",
    delivery_method:"1",
    comment:"This is okauuuu",
    _csrf:"CmpXGXln-by3UZJ8XPqAr668GOVFYvDnUUW4",
    warehouse_id:"5af1a386a9b3263e5cd5d62e",
    products:[{
        product_type:"1",
        product_id:"59ba5588472b1f5b435e991b",
        lot_number:"559/865891",
        number_items:100,
        unit_qtty:70,
        status:"1"
    }]
}
let returned_product_info,number_items_before,number_items_after;
// credentials
let supplier_Credentials={email:"prucletiki@ququb.com",password:"prucletiki@ququb.com"}
//Require the dev-dependencies
let chai = require('chai');
var agent = require('superagent').agent();
let should = chai.should();
let expect = chai.expect;
let assert = chai.assert;
let newOrder;
let newProduct;
let config= require ("./config");
let URL =config.URL;
var clone =(a)=>{
return JSON.parse(JSON.stringify(a));
}

describe('testing what a supplier can do',()=>{
    // login as supplier
    before(function(done){
        agent.post(URL+'/user.signin')
        .send(supplier_Credentials)
        .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
        });
    });
    it('it should not let a supplier create a stockIn with an invalid target_warehouse',(done)=>{
        // doing a stockIn with an invalid target_warehouse
        newProduct=clone(product_Format);
        newProduct.target_warehouse="5b1a8072df2d5d4ee2essvsbmc6"
        agent.post(URL+'/warehouse.IN')
        .send(newProduct)
        .end(function(err,response){
            expect(response.status).to.equal(400)
            done();
            })
    })
    it('it should not let a supplier create a stockIn with an empty target_warehouse',(done)=>{
        // doing a stockIn with an invalid target_warehouse
        newProduct=clone(product_Format);
        newProduct.target_warehouse=undefined
        agent.post(URL+'/warehouse.IN')
        .send(newProduct)
        .end(function(err,response){
            expect(response.status).to.equal(400)
            done();
        })
    })
    it('it should not let a supplier create a stockIn with an invalid delivery_method',(done)=>{
        // doing a stockIn with an invalid delivery_method
        newProduct=clone(product_Format);
        newProduct.delivery_method="56"
        agent.post(URL+'/warehouse.IN')
        .send(newProduct)
        .end(function(err,response){
            expect(response.status).to.equal(400)
            done();
        })
    })
    it('it should not let a supplier create a stockIn with an empty delivery_method',(done)=>{
        // doing a stockIn without delivery_method
        newProduct=clone(product_Format);
        newProduct.delivery_method=undefined
        agent.post(URL+'/warehouse.IN')
        .send(newProduct)
        .end(function(err,response){
            expect(response.status).to.equal(400)
            done();
        })
    })
    it('it should not let a supplier create a stockIn with an invalid warehouse_id',(done)=>{
        // doing a stockIn with an invalid warehouse_id
        newProduct=clone(product_Format);
        newProduct.warehouse_id="5af1a386a9b3263ehurih9353mb62e"
        agent.post(URL+'/warehouse.IN')
        .send(newProduct)
        .end(function(err,response){
            expect(response.status).to.equal(400)
            done();
        })
    })
    it('it should not let a supplier create a stockIn with an empty warehouse_id',(done)=>{
        // doing a stockIn without warehouse_id
        newProduct=clone(product_Format);
        newProduct.warehouse_id=undefined
        agent.post(URL+'/warehouse.IN')
        .send(newProduct)
        .end(function(err,response){
            expect(response.status).to.equal(400)
            done();
        })
    })
    it('it should not let a supplier create a stockIn with a warehouse_id = target_warehouse',(done)=>{
        // sdoing a stockIn where target warehouse and warehouse_id(where the products are coming from) are the same
        newProduct=clone(product_Format);
        newProduct.warehouse_id="5b1a8072df2d5d4ee2ee72c6"
        agent.post(URL+'/warehouse.IN')
        .send(newProduct)
        .end(function(err,response){
            expect(response.status).to.equal(400)
            done();
        })
    })
    it('it should not let a supplier create a stockIn with an invalid product_type',(done)=>{
        // doing a stockIn with an invalid product_type
        newProduct=clone(product_Format.products);
        newProduct.product_type= "986"
        agent.post(URL+'/warehouse.IN')
        .send(newProduct)
        .end(function(err,response){
            expect(response.status).to.equal(400)
            done();
        })
    })
    it('it should not let a supplier create a stockIn with an empty product_type',(done)=>{
        // doing a stockIn without product_type
        newProduct=clone(product_Format.products);
        newProduct.product_type=undefined
        agent.post(URL+'/warehouse.IN')
        .send(newProduct)
        .end(function(err,response){
            expect(response.status).to.equal(400)
            done();
        })
    })
    it('it should not let a supplier create a stockIn with an invalid product_id',(done)=>{
        // doing a stockIn with an invalid product_id
        newProduct=clone(product_Format.products);
        newProduct.product_id= "59b8f9c1c41343904n33d43f016"
        agent.post(URL+'/warehouse.IN')
        .send(newProduct)
        .end(function(err,response){
            expect(response.status).to.equal(400)
            done();
        })
    })
    it('it should not let a supplier create a stockIn with an empty lot_number',(done)=>{
        // doing a stockIn without the lot_number
        newProduct=clone(product_Format.products);
        newProduct.lot_number=undefined
        agent.post(URL+'/warehouse.IN')
        .send(newProduct)
        .end(function(err,response){
            expect(response.status).to.equal(400)
            done();
        })
    })
    it('it should not let a supplier create a stockIn with an empty number_items',(done)=>{
        // doing a stockIn without the number_items
        newProduct=clone(product_Format.products);
        newProduct.number_items=undefined
        agent.post(URL+'/warehouse.IN')
        .send(newProduct)
        .end(function(err,response){
            expect(response.status).to.equal(400)
            done();
        })
    })
    it('it should not let a supplier create a stockIn with an empty unit_qtty',(done)=>{
        // doing a stockIn without qtty
        newProduct=clone(product_Format.products);
        newProduct.unit_qtty=undefined
        agent.post(URL+'/warehouse.IN')
        .send(newProduct)
        .end(function(err,response){
            expect(response.status).to.equal(400)
            done();
        })
    })
    it('it should not let a supplier create a stockIn with an invalid unit_qtty',(done)=>{
        // doing a stockIn with an invalid unit_qtty
        newProduct=clone(product_Format.products);
        newProduct.unit_qtty="56767"
        agent.post(URL+'/warehouse.IN')
        .send(newProduct)
        .end(function(err,response){
            expect(response.status).to.equal(400)
            done();
        })
    })
    it('it should not let a supplier create a stockIn with an invalid status',(done)=>{
        // sdoing a stockIn with an invalid status
        newProduct=clone(product_Format.products);
        newProduct.status="56767"
        agent.post(URL+'/warehouse.IN')
        .send(newProduct)
        .end(function(err,response){
            expect(response.status).to.equal(400)
            done();
        })
    })
    it('it should not let a supplier create a stockIn with an empty comment',(done)=>{
        // doing a stockIn without comments
        newProduct=clone(product_Format.products);
        newProduct.comment=undefined
        agent.post(URL+'/warehouse.IN')
        .send(newProduct)
        .end(function(err,response){
            expect(response.status).to.equal(400)
            done();
        })
    })
    it("it should let a supplier get a product details",(done)=>{
        // viewing the product details as a supplier before stockIn
        agent.get(URL+"/product.stock.info/5af1a04ea9b3263e5cd5d62c"+"/"+product_Format.warehouse_id+"/"+product_Format.products[0].product_id)
        .end((err,response)=>{
            //console.log(response.text)
            returned_product_info=JSON.parse(response.text);
            number_items_before=returned_product_info
            number_items_before.forEach(element => {
                if(element._id.lot_number==product_Format.products[0].lot_number && element._id.product_id==product_Format.products[0].product_id && element._id.status==product_Format.products[0].status){
                    number_items_before=element.number_items
                    console.log("number of items before stock In"+element.number_items)
                }
            });
            expect(response.status).to.equal(200)
            done();
        })
    })
    it("it should let a supplier make a stock in",(done)=>{
        // doing a stockIn as a supplier
        agent.post(URL+"/warehouse.IN")
        .send(product_Format)
        .end((err,response)=>{
            expect(response.status).to.equal(200)
            done()
        })
    })
    it("it should let a supplier get a product details after stockIn",(done)=>{
        // viewing the product details after stockIn
        agent.get(URL+"/product.stock.info/5af1a04ea9b3263e5cd5d62c"+"/"+product_Format.warehouse_id+"/"+product_Format.products[0].product_id)
        .end((err,response)=>{
            returned_product_info=null;
            returned_product_info=JSON.parse(response.text);
            number_items_after=returned_product_info;
            number_items_after.forEach(element => {
                if(element._id.lot_number==product_Format.products[0].lot_number && element._id.product_id==product_Format.products[0].product_id && element._id.status==product_Format.products[0].status){
                    number_items_after=element.number_items
                    console.log("number of items after stock In"+element.number_items)
                }
            });
            expect(response.status).to.equal(200)
            done();
        })
    })
    it("The stock number_items before stockIn should be less than number_items after stockIn",(done)=>{
        // comparing the stock before stockIn and after stockIn to check if it changed
        expect(number_items_before).to.not.equal(number_items_after);
        done()
    })
})