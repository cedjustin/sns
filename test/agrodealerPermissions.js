//require dev dependencies
let chai = require('chai');
var agent = require('superagent').agent();
let async = require('async')
let config= require ("./config");
let should = chai.should();
let expect = chai.expect;
let assert = chai.assert;
let URL = config.URL;

// Credentials
let AD_Credentials ={email:"tunacikone@hvzoi.com",password:"tunacikone@hvzoi.com"},
  SUPPLIER_Credentials ={email:"prucletiki@ququb.com",password:"prucletiki@ququb.com"},
  Distributor_credentials={email:"bilihuchot@2anom.com",password:"bilihuchot@2anom.com"},
  Teller_credentials={email:"beruihuchot@2anome.com",password:"beruihuchot@2anome.com"};


describe('Testing Agro-Dealer permissions', ()=> {
	
	// Loging into the system
	before(function(done){
		agent.post(URL+'/user.signin')
		.send(AD_Credentials)
		.end(function(err, response){
			expect(response.statusCode).to.equal(200);
			done();
		});
	});

	// WAREHOUSE LIST PAGE
	it("It should let an Agro-Dealer access Warehouse list page",(done)=> {
		agent.get(URL+"/warehouse.list/5af0207f5edecc3124c37ca6")
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		});
	});

	// PRODUCT STATUS LIST
	it("It should let an Agro-Dealer access Product Status List",(done)=> {
		agent.get(URL+"/product.status.list")
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		});
	});

	// PRODUCT TYPE LIST
	it("It should let an Agro-Dealer access Product type List",(done)=> {
		agent.get(URL+"/product.type.list")
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		});
	});

	// PRODUCT DELIVERY METHOD
	it("It should let an Agro-Dealer access Production Delivery Method",(done)=> {
		agent.get(URL+"/product.delivery.method")
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		});
	});

	// PRODUCT INVENTORY CASE
	it("It should let an Agro-Dealer access Product Inventory Case",(done)=> {
		agent.get(URL+"/product.inventory.case/")
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		});
	});

	// PRODUCT LIST
	it("It should let an Agro-Dealer access Product List",(done)=> {
		agent.get(URL+"/warehouse.product.list/5af0207f5edecc3124c37ca6/-1")
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		});
	});

	// LIST OF WAREHOUSES
	it("It should let an Agro-Dealer access Warehouse History",(done)=> {
		agent.get(URL+"/warehouse.all.history.list/5af0207f5edecc3124c37ca6")
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		});
	});

	// WAREHOUSE PRODUCTS LIST
	it("It should let an Agro-Dealer access List of Products in WareHouse",(done)=> {
		agent.get(URL+"/warehouse.product.list/5af0207f5edecc3124c37ca6/5af022b6389c7231fb403312")
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		});
	});

	// WAREHOUSE HISTORY LIST FOR DIFFERENT WAREHOUSES
	it("It should let an Agro-Dealer access Warehouse History List",(done)=> {
		agent.get(URL+"/warehouse.history.list/5af022b6389c7231fb403312")
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		});
	});

	// WAREHOUSE PRODUCTION LIST FOR DIFFERENT WAREHOUSES
	it("It should let an Agro-Dealer access Product List",(done)=> {
		agent.get(URL+"/warehouse.product.list/5af0207f5edecc3124c37ca6/5af022c7389c7231fb403314")
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		});
	});

	// WAREHOUSE HISTORY LIST FOR DIFFERENT PRODUCTS
	it("It should let an Agro-Dealer access Warehouse history list",(done)=> {
		agent.get(URL+"/warehouse.history.list/5af022c7389c7231fb403314")
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		});
	});

	// NOTIFICATION PAGE
	it("It should let an Agro-Dealer access Notification page",(done)=> {
		agent.get(URL+"/notif.list")
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		});
	});

	// USER TYPE LIST
	it("It should let an Agro-Dealer access User Type List",(done)=> {
		agent.get(URL+"/user.type.list")
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		});
	});

	// FREQUENTLY ASKED QUESTION LISTS
	it("It should let an Agro-Dealer access Frequently Asked Question List",(done)=> {
		agent.get(URL+"/faq.list")
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		});
	});

	// MESSAGE (QUESTIONS) LIST
	it("It should let an Agro-Dealer access A list of Messages (Questions)",(done)=> {
		agent.get(URL+"/message.list")
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		});
	});

	// This place is reserved for sending a message to the 
	// server as part of the Frequently Asked Questions

	it("It should let an Agro-Dealer access Answers to Asked Questions",(done)=> {
		agent.get(URL+"/message.answers.list/5b17d6f78b9c4c7b5bb7e72c")
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		});
	});
	/*
	* 	USER PROFILE REQUESTS
	*	MAP_TYPE_LIST
	*	USER_TYPE_LIST
	*	USER_INFO
	*/

	// PROFILE INFORMATION
	it("It should let an Agro-Dealer access Their own profile information",(done)=> {
		agent.get(URL+"/user.info/5af02241389c7231fb40330f")
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		});
	});

	// MAP_TYPE_LIST INFORMATION (PAGE)
	it("It should let an Agro-Dealer access MAP_TYPE_LIST info",(done)=> {
		agent.get(URL+"/map.type.list")
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		});
	});

	// USER_TYPE_LIST INFORMATION (PAGE)
	it("It should let an Agro-Dealer access USER_TYPE_LIST",(done)=> {
		agent.get(URL+"/user.type.list")
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		});
	});
});