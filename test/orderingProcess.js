//require dev dependencies
let chai = require('chai');
var agent = require('superagent').agent();
let async = require('async')
let config= require ("./config");
let should = chai.should();
let expect = chai.expect;
let assert = chai.assert;
let orderFormat={
  "isSubsidized":true,
  "isSelfPicking":true,
  "originOrg_id":"5af1a04ea9b3263e5cd5d62c",
  "originWH_id":"5af1a386a9b3263e5cd5d62e",
  "offloadingMargin":"15",
  "driver":"fab",
  "plate":"rac672d",
  orders:[
  {
  "minMargin_AD":40,
  "names":"ks njoro ii",
  "product_type":1,
  "product_id":"59bd5ba899573f31134b71b7",
  "quantity":7
  }
  ],
  "destOrg_id":"5af0207f5edecc3124c37ca6",
  "destWH_id":"5af022b6389c7231fb403312",
  "_csrf":"u8tm27QB-KqKD0r4P6TgGGQC6mtjOxRDyT0Q"
}


var supplier_stock_before_picked = 0;
var supplier_stock_after_picked = 0;

var AD_stock_before = 0;
var AD_stock_after = 0;


var picked_lots = [];

var clone =(a)=>{
  return JSON.parse(JSON.stringify(a));
}
let AD_Credentials ={email:"tunacikone@hvzoi.com",password:"tunacikone@hvzoi.com"},
  SUPPLIER_Credentials ={email:"prucletiki@ququb.com",password:"prucletiki@ququb.com"},
  Distributor_credentials={email:"bilihuchot@2anom.com",password:"bilihuchot@2anom.com"},
  Teller_credentials={email:"beruihuchot@2anome.com",password:"beruihuchot@2anome.com"},
  SA_Credentials={email:"jeandamantaka@yahoo.com",password:"b88692447d933095a95515c0eb0f7f0382acc36ec4c894057db1379e6b18e51ba"}
let URL =config.URL;
let ObjectID=config.ObjectID;
let order_Id,totalPrice;
let pickedLot_id,pickedLot_items,pickedLot_unitQtty,pickedLot_qtty;
let product_info;
describe('Testing creation',()=>{
  /*
  1. Agrodealer login
  2. Place an order and get the order_id
  3. LOgin as Distributor
  4. Distributor approves order
  5. Login as teller 
  6. Confirm order
  7. Login as supplier
  8. Release the order..
*/
  before((done)=>{//1. Agrodealer login
    agent.post(URL+'/user.signin')
      .send(AD_Credentials)
      .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
      });
  });

  //2. Place an order and get the order_id
  it("it should let an agroDealer make an order",(done)=>{//
    agent.post(URL+'/order.new')
    .send(orderFormat)
    .end((err,response)=>{
      order_Id=config.stripquotes(response.text)//ObjectID("5b3dc4f734391e1a494e2303");
      //(">>>>>>"+" >> "+order_Id)
      expect(response.status).to.equal(200);
      done();
    })
  });

  // Agro-dealers' stock before the orders is confirmed.
  it("It should let an agro-dealer check stock info before an order is made", (done)=>{
    agent.get(URL+"/product.stock.info/"+orderFormat.originOrg_id+"/" 
            +orderFormat.destWH_id + "/"+orderFormat.orders[0].product_id)
    .end(function(err, response){
      let warehouse = JSON.parse(response.text);
      AD_stock_before = warehouse[0].good_items * warehouse[0].unit_qtty;
      expect(response.statusCode).to.equal(200);
      done();
    })
  })
})
describe('Testing approval to paid',function(){
  this.timeout(5000);
  before((done)=>{//3. Login as Distributor
    agent.post(URL+'/user.signin')
      .send(Distributor_credentials)
      .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
      });
  });
  it("it should let a distributor approve an order",(done)=>{
    agent.put(URL+'/order.approve')
    .send({order_id:order_Id})
    .end((err,response)=>{
      //('>>>'+JSON.stringify(response.text))
      expect(response.statusCode).to.equal(200)
      done();
    })
  });
  before((done)=>{//3. ReLogin as Distributor
    agent.post(URL+'/user.signin')
      .send(Distributor_credentials)
      .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
      });
  });

  it("it should let a distributor get the order details",(done)=>{
    agent.get(URL+'/order.details/'+order_Id)
    .end((err,response)=>{
      //(">> "+JSON.stringify(response.text,null,2));
      let data =JSON.parse(response.text);
      expect(response.status).to.equal(200);
      expect(data).to.have.property('totalPrice')
      totalPrice =data.totalPrice;
      done();
    })
  });

  it("it should let a teller confirm the payment an order",(done)=>{//6. Confirm order
    agent.post(URL+'/order.payment.confirmation')
    .send({
      order_id:order_Id,
      amount:totalPrice,
      bankSlip:config.genUID(12),
      transaction_ref:config.genUID(10),
      when:new Date(),
      isValid:true,
      authorization_token:config.AUTH_TOKEN
    })
    .end((err,response)=>{
      //('>>>'+JSON.stringify(response,null,2))
      expect(response.status).to.equal(200);
      done();
    })
  })
})
describe("Checking the ordering process after the order reached to the supplier",()=>{
  before((done)=>{//3. ReLogin as Distributor
    agent.post(URL+'/user.signin')
      .send(SUPPLIER_Credentials)
      .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
      });
  });

  it("supplier should get the stock details",(done)=>{
    agent.get(URL+"/product.stock.info/"+orderFormat.originOrg_id+"/-1/"+orderFormat.orders[0].product_id)
    .end((err,response)=>{
	    expect(response.status).to.equal(200);
	    product_info = JSON.parse(response.text);

	    // Declaring variables to support loop
	    var picked_qtty = 0;
	    var qtty_to_pic = orderFormat.orders[0].quantity;

      	// JSONObject Creation
	    for(var i = 0; i < product_info.length; i++) {
	      	if(qtty_to_pic == picked_qtty)
	      		break;
	      	var unit_qtty = product_info[i].unit_qtty;
	      	var number_items = product_info[i].number_items;
	      	var qtty_in_store = product_info[i].unit_qtty * product_info[i].number_items;

	      	if(product_info[i]._id.product_id == orderFormat.orders[0].product_id && 
	      				product_info[i]._id.status == 1) {
	      		if((qtty_to_pic - picked_qtty) >= qtty_in_store) {
		      		picked_qtty += qtty_in_store;
		      		picked_lots.push({
		      			"id": product_info[i]._id.lot_number,
		      			"items": number_items,
		      			"unit_qtty": unit_qtty,
		      			"qtty": qtty_in_store
		      		});
	      		} else if((qtty_to_pic - picked_qtty) < qtty_in_store) {
		      		var taken = (qtty_to_pic - picked_qtty);
		      		picked_qtty += taken;
		      		picked_lots.push({
		      			"id": product_info[i]._id.lot_number,
		      			"items": (taken / unit_qtty),
		      			"unit_qtty": unit_qtty,
		      			"qtty": taken
		      		});
		      	}
	      	}
	    }

      // Handling 
      if(picked_qtty < qtty_to_pic)
        console.log("Small quantity in Suppliers Wallet");
    	done();
      for (var i = 0; i < product_info.length; i++) {
        supplier_stock_before_picked += product_info[i].good_items * product_info[i].unit_qtty;
      }
    });
  })
  it("it should let a supplier declare a product as picked",(done)=>{
    agent.put(URL+"/order.pick")
    .send({
        "order_id": order_Id,
        "newLots": [
           {
              "pickedLots": picked_lots,
              "product_id": orderFormat.orders[0].product_id
           }
        ]
    })   
    .end((err,response)=>{
      //(JSON.stringify(response.text))
      expect(response.status).to.equal(200);
      done()
    });
  });

  it("The difference between stocks after picking should be equal to the ordered quantity", (done)=>{
    agent.get(URL+"/product.stock.info/"+orderFormat.originOrg_id+"/-1/"+orderFormat.orders[0].product_id)
    .end(function(err, response) {

      // Testing stock differences before and after picked
      product_details = JSON.parse(response.text);

      for(var i = 0; i < product_details.length; i++) {
        supplier_stock_after_picked += product_details[i].good_items * product_details[i].unit_qtty;
      }
      expect(supplier_stock_before_picked - supplier_stock_after_picked).to.equal(orderFormat.orders[0].quantity);
      done();
    });  
  });


  before((done)=>{//1. Agrodealer login
    agent.post(URL+'/user.signin')
      .send(AD_Credentials)
      .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
      });
  });
  URL+"/order.arrived/"+order_Id
  it("It should let an agroDealer set orderStatus as arrived",(done)=>{
    agent.get(URL+"/order.arrived/"+order_Id)
    .end((err,response)=>{
      //(URL+"/order.arrived/"+order_Id)
      expect(response.status).to.equal(200);
      done();
    })
  })
})

describe("testing after the order has been declared as arrived",()=>{
   before((done)=>{
    agent.post(URL+'/user.signin')
      .send(SA_Credentials)
      .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
      });
  });

  it("It should let Sector agronomist confirm an order",(done)=>{
    agent.post(URL+"/order.confirm")
    .send({"lostPickedLots":[],"order_id":order_Id})
    .end((err,response)=>{
      //(response.text)
      expect(response.status).to.equal(200);
      done();
    })
  })
})


describe("Testing stock changes for the Agrodealer after agronomist confirmation", (done)=> {
  before((done)=> {
    agent.post(URL+'/user.signin')
      .send(AD_Credentials)
      .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
      })
  });

  it("AD should declare the products as arrived", (done)=> {
    agent.get(URL+"/order.delivered/"+ order_Id)
    .end(function(err, response) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  it("The stock of an agroDealer should increase after the sector agronomist confirms the order", (done)=>{
    agent.get(URL+"/product.stock.info/"+orderFormat.originOrg_id+"/" 
            +orderFormat.destWH_id + "/"+orderFormat.orders[0].product_id)
    .end(function(err, response){
      let warehouse_details = JSON.parse(response.text);
      AD_stock_after = warehouse_details[0].unit_qtty * warehouse_details[0].good_items;
      expect(AD_stock_after - AD_stock_before).to.equal(orderFormat.orders[0].quantity);
      done();
    })
  })

  it("The pickedLots should match picked_lots' ids from the stock",(done)=>{
    agent.get(URL+'/order.details/'+order_Id)
    .end((err,response)=>{
      //(">> "+JSON.stringify(response.text,null,2));
      let data =JSON.parse(response.text);
      var pickedLots = data.ordered[0].pickedLots;
      for(var i = 0; i < pickedLots.length; i++)
        expect(pickedLots[i].id).to.equal(picked_lots[i].id);
      done();
    })
  });

});








