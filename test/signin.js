let signinFormat={
  email:"tunacikone@hvzoi.com",
  password:"tunacikone@hvzoi.com"
}
//Require the dev-dependencies
let chai = require('chai');
var agent = require('superagent').agent();
let should = chai.should();
let expect = chai.expect;
let assert = chai.assert;
let config= require ("./config");
var clone =(a)=>{
  return JSON.parse(JSON.stringify(a));
}
let newUser;
let URL =config.URL;
describe('testing how users can signin',()=>{
  it('1. It should not let a user login using an invalid email(numbers)',(done)=>{
    newUser=clone(signinFormat);
    newUser.email=3758758132;
    agent.post(URL+'/user.signin')
    .send(newUser)
    .end(function(err, response){
      expect(response.statusCode).to.equal(400);
      done();
    });
  })
  it('2. It should not let a user login using an invalid email(special characters)',(done)=>{
    newUser=clone(signinFormat);
    newUser.email="OR 1=1"
    agent.post(URL+'/user.signin')
    .send(newUser)
    .end(function(err, response){
      expect(response.statusCode).to.equal(400);
      done();
    });
  })
  it('3. It should not let a user login using an email and invalid password',(done)=>{
    newUser=clone(signinFormat);
    newUser.password="tunacikone@hvzoi.cam"
    agent.post(URL+'/user.signin')
    .send(newUser)
    .end(function(err, response){
      expect(response.statusCode).to.equal(400);
      done();
    });
})
    it('4. It should not let a user login using an email and an empty password',(done)=>{
      newUser=clone(signinFormat);
      newUser.password=undefined;
      agent.post(URL+'/user.signin')
      .send(newUser)
      .end(function(err, response){
        expect(response.statusCode).to.equal(400);
        done();
      });
    })
    it('5. It should not let a user login using an invalid email and correct password',(done)=>{
      newUser=clone(signinFormat);
      newUser.email="tunacikone@hvzoi.con";
      agent.post(URL+'/user.signin')
      .send(newUser)
      .end(function(err, response){
        expect(response.statusCode).to.equal(400);
        done();
      });
    })
    it('6. It should not let a user login using an empty email and correct password',(done)=>{
      newUser=clone(signinFormat);
      newUser.email=undefined;
      agent.post(URL+'/user.signin')
      .send(newUser)
      .end(function(err, response){
        expect(response.statusCode).to.equal(400);
        done();
      });
    })
  it('7. It should not let a user login using an account which has not been validated yet',(done)=>{
    newUser=clone(signinFormat);
    newUser.email="cedroced@gmail.com",
    newUser.password="1234567890"
    agent.post(URL+'/user.signin')
    .send(newUser)
    .end(function(err, response){
      expect(response.statusCode).to.equal(400);
      done();
    });
  });

  // User with special character in domain name
  it('8. It should not let in a user with a special character in the domain name of the email',(done)=>{
    newUser=clone(signinFormat);
    newUser.email="tunacikone@*$**.com",
    newUser.password="tunacikone@hvzoi.com"
    agent.post(URL+'/user.signin')
    .send(newUser)
    .end(function(err, response){
      expect(response.statusCode).to.equal(400);
      done();
    });
  })
  // User with no credentials
  it('9. It should not let in a user with no credentials',(done)=>{
    newUser=clone(signinFormat);
    newUser.email="",
    newUser.password=""
    agent.post(URL+'/user.signin')
    .send(newUser)
    .end(function(err, response){
      expect(response.statusCode).to.equal(400);
      done();
    });
  })

  // Testing User with Correct Credentials
  it('10. It should let a user(Agrodealer) with correct credentials to login', (done) => {
    newUser=clone(signinFormat);
    newUser.email="tunacikone@hvzoi.com";
    newUser.password="tunacikone@hvzoi.com";
    agent.post(URL+'/user.signin')
    .send(newUser)
    .end(function(err, response) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  // Testing User with Correct Credentials
  it('11. It should let a user(Supplier) with correct credentials to login', (done) => {
    newUser=clone(signinFormat);
    newUser.email="prucletiki@ququb.com";
    newUser.password="prucletiki@ququb.com";
    agent.post(URL+'/user.signin')
    .send(newUser)
    .end(function(err, response) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  // Testing User with Correct Credentials
  it('12. It should let a user(Distributor) with correct credentials to login', (done) => {
    newUser=clone(signinFormat);
    newUser.email="bilihuchot@2anom.com";
    newUser.password="bilihuchot@2anom.com";
    agent.post(URL+'/user.signin')
    .send(newUser)
    .end(function(err, response) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  // Testing User with Correct Credentials
  it('13. It should let a user(Teller) with correct credentials to login', (done) => {
    newUser=clone(signinFormat);
    newUser.email="beruihuchot@2anome.com";
    newUser.password="beruihuchot@2anome.com";
    agent.post(URL+'/user.signin')
    .send(newUser)
    .end(function(err, response) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });
})
