let userFormat={
  email:"fabyeezy@rw.rw",
  foreName:"fa",
  org_id:"59b8d6a5feedac4b03fe29a1",
  password:"1234567890",
  password2:"1234567890",
  phone_number:"0722384756",
  sex:"m",
  surname:"yeezy",
  terms_and_conditions:true,
  type_org:"3"
}
//Require the dev-dependencies
let chai = require('chai');
var agent = require('superagent').agent();
let should = chai.should();
let expect = chai.expect;
let assert = chai.assert;
let config= require ("./config");
var clone =(a)=>{
  return JSON.parse(JSON.stringify(a));
}
let newUser;
let URL =config.URL;
describe('testing signUp page',()=>{
  it("it should not let a user signUp(register) with an empty foreName",(done)=>{
    newUser=clone(userFormat);
    newUser.foreName=undefined
    agent.post(URL+'/user.signup')
    .send(newUser)
    .end(function(err,response){
      expect(response.status).to.equal(400);
      done();
    })
  })
  it("it should not let a user signUp(register) with an empty surname",(done)=>{
    newUser=clone(userFormat);
    newUser.surname=undefined
    agent.post(URL+'/user.signup')
    .send(newUser)
    .end(function(err,response){
      expect(response.status).to.equal(400);
      done();
    })
  })
  it("it should not let a user signUp(register) with an empty email",(done)=>{
    newUser=clone(userFormat);
    newUser.email=undefined
    agent.post(URL+'/user.signup')
    .send(newUser)
    .end(function(err,response){
      expect(response.status).to.equal(400);
      done();
    })
  })
  it("it should not let a user signUp(register) with an invalid email",(done)=>{
    newUser=clone(userFormat);
    newUser.email="fabyeezyrw.rw"
    agent.post(URL+'/user.signup')
    .send(newUser)
    .end(function(err,response){
      expect(response.status).to.equal(400);
      done();
    })
  })
  it("it should not let a user signUp(register) with an empty password",(done)=>{
    newUser=clone(userFormat);
    newUser.password=undefined
    agent.post(URL+'/user.signup')
    .send(newUser)
    .end(function(err,response){
      expect(response.status).to.equal(400);
      done();
    })
  })
  it("it should not let a user signUp(register) with an empty password2",(done)=>{
    newUser=clone(userFormat);
    newUser.password2=undefined
    agent.post(URL+'/user.signup')
    .send(newUser)
    .end(function(err,response){
      expect(response.status).to.equal(400);
      done();
    })
  })
  it("it should not let a user signUp(register) with password != password2",(done)=>{
    newUser=clone(userFormat);
    newUser.password="1234567890"
    newUser.password2="338913081"
    agent.post(URL+'/user.signup')
    .send(newUser)
    .end(function(err,response){
      expect(response.status).to.equal(400);
      done();
    })
  })
  it("it should not let a user signUp(register) with an empty phone_number",(done)=>{
    newUser=clone(userFormat);
    newUser.phone_number=undefined
    agent.post(URL+'/user.signup')
    .send(newUser)
    .end(function(err,response){
      expect(response.status).to.equal(400);
      done();
    })
  })
  it("it should not let a user signUp(register) with an empty sex",(done)=>{
    newUser=clone(userFormat);
    newUser.sex=undefined
    agent.post(URL+'/user.signup')
    .send(newUser)
    .end(function(err,response){
      expect(response.status).to.equal(400);
      done();
    })
  })
  it("it should not let a user signUp(register) with an invalid sex",(done)=>{
    newUser=clone(userFormat);
    newUser.phone_number="S"
    agent.post(URL+'/user.signup')
    .send(newUser)
    .end(function(err,response){
      expect(response.status).to.equal(400);
      done();
    })
  })
  it("it should not let a user signUp(register) with an empty org_id",(done)=>{
    newUser=clone(userFormat);
    newUser.org_id=undefined
    agent.post(URL+'/user.signup')
    .send(newUser)
    .end(function(err,response){
      expect(response.status).to.equal(400);
      done();
    })
  })
  it("it should not let a user signUp(register) with an invalid org_id",(done)=>{
    newUser=clone(userFormat);
    newUser.org_id="59b8d6a5feedac4b03fe2782842"
    agent.post(URL+'/user.signup')
    .send(newUser)
    .end(function(err,response){
      expect(response.status).to.equal(400);
      done();
    })
  })
  it("it should not let a user signUp(register) with an empty type_org",(done)=>{
    newUser=clone(userFormat);
    newUser.type_org=undefined
    agent.post(URL+'/user.signup')
    .send(newUser)
    .end(function(err,response){
      expect(response.status).to.equal(400);
      done();
    })
  })
  it("it should not let a user signUp(register) with an empty type_org",(done)=>{
    newUser=clone(userFormat);
    newUser.type_org=undefined
    agent.post(URL+'/user.signup')
    .send(newUser)
    .end(function(err,response){
      expect(response.status).to.equal(400);
      done();
    })
  })
  it("it should not let a user signUp(register) with an invalid type_org",(done)=>{
    newUser=clone(userFormat);
    newUser.type_org=-39284982
    agent.post(URL+'/user.signup')
    .send(newUser)
    .end(function(err,response){
      expect(response.status).to.equal(400);
      done();
    })
  })
  it("it should not let a user signUp(register) without accepting the terms_and_conditions",(done)=>{
    newUser=clone(userFormat);
    newUser.terms_and_conditions=false
    agent.post(URL+'/user.signup')
    .send(newUser)
    .end(function(err,response){
      expect(response.status).to.equal(400);
      done();
    })
  })
})
