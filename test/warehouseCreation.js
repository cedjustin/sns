//warehouse_format(structure)
let warehouse_Format = {
  "name":"warehouseTest13",
  "prov_id":"1",
  "dist_id":"1",
  "sect_id":"2",
  "cell_id":"9",
  "vill_id":"64",
  "loadingMargin":10
}
//Require the dev-dependencies
let chai = require('chai');
var agent = require('superagent').agent();
let should = chai.should();
let expect = chai.expect;
let assert = chai.assert;
let newWarehouse;
let config= require ("./config");
var clone =(a)=>{
  return JSON.parse(JSON.stringify(a));
}
let AD_Credentials ={email:"tunacikone@hvzoi.com",password:"tunacikone@hvzoi.com"},
  SUPPLIER_Credentials ={email:"prucletiki@ququb.com",password:"prucletiki@ququb.com "},
  Distributor_credentials={email:"bilihuchot@2anom.com",password:"bilihuchot@2anom.com"},
  Teller_credentials={email:"beruihuchot@2anome.com",password:"beruihuchot@2anome.com"},
  RAB_credentials={email:"egide.gatari@rab.gov.rw",password:"b88692447d933095a95515c0eb0f7f0382acc36ec4c894057db1379e6b18e51ba"},
  SA_credentials={email:"jeandamantaka@yahoo.com",password:"b88692447d933095a95515c0eb0f7f0382acc36ec4c894057db1379e6b18e51ba"},  
  DistrictA_credentials={email:"tgatoya@gmail.com",password:"b88692447d933095a95515c0eb0f7f0382acc36ec4c894057db1379e6b18e51ba"}  
  let URL =config.URL;
//tests about creation of warehouses
describe("testing the creation of warehouses",()=>{
  before((done)=>{
    agent.post(URL+"/user.signin")
    .send(AD_Credentials)
    .end((err,response)=>{
      expect(response.statusCode).to.equal(200);
      done();
    })
  });
  //these tests should return status (400)
  //sending the warehouse format with an invalid prov_id
  it('it should not create a new warehouse with an invalid prov_id',(done)=>{
        newWarehouse=clone(warehouse_Format);
        newWarehouse.prov_id=35;
        agent.post(URL+'/warehouse.new').send(newWarehouse).end((err, res) => {
          console.log(res.text);
         	expect(res.text).to.exist;
          expect(res.status).to.equal(400);
          done();
        });
      })
  it('it should not create a new warehouse with an empty prov_id',(done)=>{
    //sending the warehouse format without the prov_id
    newWarehouse=clone(warehouse_Format);
    newWarehouse.prov_id=undefined;
    agent.post(URL+'/warehouse.new').send(newWarehouse).end((err, res) => {
      expect(res.text).to.exist;
      expect(res.status).to.equal(400);
      done();
    });
  })
  it('it should not create a new warehouse with an invalid dist_id',(done)=>{
    //sending the warehouse format with an invalid dist_id
    newWarehouse=clone(warehouse_Format);
    newWarehouse.dist_id=10035;
    agent.post(URL+'/warehouse.new').send(newWarehouse).end((err, res) => {
      expect(res.text).to.exist;
      expect(res.status).to.equal(400);
      done();
    });
  })
  it('it should not create a new warehouse with an empty dist_id',(done)=>{
    //sending the warehouse format without the dist_id
    newWarehouse=clone(warehouse_Format);
    newWarehouse.dist_id=undefined;
    agent.post(URL+'/warehouse.new').send(newWarehouse).end((err, res) => {
      expect(res.text).to.exist;
      expect(res.status).to.equal(400);
      done();
    });
  })
  it('it should not create a new warehouse with an invalid sect_id',(done)=>{
    //sending the warehouse format with an invalid the sect_id
    newWarehouse=clone(warehouse_Format);
    newWarehouse.sect_id=10035;
    agent.post(URL+'/warehouse.new').send(newWarehouse).end((err, res) => {
      expect(res.text).to.exist;
      expect(res.status).to.equal(400);
      done();
    });
  })
  it('it should not create a new warehouse with an empty sect_id',(done)=>{
    //sending the warehouse format without the sect_id
    newWarehouse=clone(warehouse_Format);
    newWarehouse.sect_id=undefined;
    agent.post(URL+'/warehouse.new').send(newWarehouse).end((err, res) => {
      expect(res.text).to.exist;
      expect(res.status).to.equal(400);
      done();
    });
  })
  it('it should not create a new warehouse with an invalid cell_id',(done)=>{
    //sending the warehouse format with an invalid the cell_id
    newWarehouse=clone(warehouse_Format);
    newWarehouse.cell_id=10035;
    agent.post(URL+'/warehouse.new').send(newWarehouse).end((err, res) => {
      expect(res.text).to.exist;
      expect(res.status).to.equal(400);
      done();
    });
  })
  it('it should not create a new warehouse with an empty cell_id',(done)=>{
    //sending the warehouse format without the cell_id
    newWarehouse=clone(warehouse_Format);
    newWarehouse.cell_id=undefined;
    agent.post(URL+'/warehouse.new').send(newWarehouse).end((err, res) => {
      expect(res.text).to.exist;
      expect(res.status).to.equal(400);
      done();
    });
  })
  it('it should not create a new warehouse with an invalid vill_id',(done)=>{
    //sending the warehouse format with an invalid the vill_id
    newWarehouse=clone(warehouse_Format);
    newWarehouse.vill_id=10035;
    agent.post(URL+'/warehouse.new').send(newWarehouse).end((err, res) => {
      expect(res.text).to.exist;
      expect(res.status).to.equal(400);
      done();
    });
  })
  it('it should not create a new warehouse with an empty vill_id',(done)=>{
    //sending the warehouse format without the vill_id
    newWarehouse=clone(warehouse_Format);
    newWarehouse.vill_id=undefined;
    agent.post(URL+'/warehouse.new').send(newWarehouse).end((err, res) => {
      expect(res.text).to.exist;
      expect(res.status).to.equal(400);
      done();
    });
  })
})

//authority testing
describe('Testing who is authorised to create a warehouse', ()=> {
  before((done)=>{
    agent.post(URL+'/user.signin')
	    .send(Distributor_credentials)
	    .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
  })
})
  it('it should not allow the distributor to create a warehouse',(done)=>{
        agent.post(URL+'/warehouse.new')
        .send(warehouse_Format)
        .end((err, res)=>{
          console.log(res.text);
          expect(res.text).to.exist;
          expect(res.status).to.equal(400);
          done();
	    })
    })
  before((done)=>{
    agent.post(URL+'/user.signin')
	    .send(Teller_credentials)
	    .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
  })
})
  it('it should not allow the teller to create a warehouse',(done)=>{
        agent.post(URL+'/warehouse.new')
        .send(warehouse_Format)
        .end((err, res)=>{
          expect(res.text).to.exist;
          expect(res.status).to.equal(400);
          done()
	    })
    })
    // before((done)=>{
    //   agent.post(URL+'/user.signin')
    //     .send(SA_credentials)
    //     .end(function(err, response){
    //       expect(response.statusCode).to.equal(200);
    //       done();
    //   })
    // })
    // it('it should not allow a sector_Agronomist to create a warehouse',(done)=>{
    //       agent.post(URL+'/warehouse.new')
    //       .send(warehouse_Format)
    //       .end((err, res)=>{
    //         expect(res.text).to.exist;
    //         expect(res.status).to.equal(400);
    //         done()
    //     })
    //   })
  //   before((done)=>{
  //     agent.post(URL+'/user.signin')
  //       .send(DistrictA_credentials)
  //       .end(function(err, response){
  //         expect(response.statusCode).to.equal(200);
  //         done();
  //   })
  // })
  //   it('it should not allow the district_Agronomist to create a warehouse',(done)=>{
  //         agent.post(URL+'/warehouse.new')
  //         .send(warehouse_Format)
  //         .end((err, res)=>{
  //           console.log(res.text);
  //           expect(res.text).to.exist;
  //           expect(res.status).to.equal(400);
  //           done()
  //       })
  //     })
  //   before((done)=>{
  //     agent.post(URL+'/user.signin')
  //       .send(RAB_credentials)
  //       .end(function(err, response){
  //         expect(response.statusCode).to.equal(200);
  //         done();
  //   })
  // })
  //   it('it should not allow a RAB agent to create a warehouse',(done)=>{
  //         agent.post(URL+'/warehouse.new')
  //         .send(warehouse_Format)
  //         .end((err, res)=>{
  //           expect(res.text).to.exist;
  //           expect(res.status).to.equal(400);
  //           done()
  //       })
  //     })
  before((done)=>{
    agent.post(URL+"/user.signin")
    .send(AD_Credentials)
    .end((err,response)=>{
      expect(response.statusCode).to.equal(200);
      done();
    })
  });
  // this a working test, it should return status(200)
  it("it should let an agro_dealer create a warehouse",(done)=>{
    agent.post(URL+"/warehouse.new")
    .send(warehouse_Format)
    .end((err,response)=>{
      console.log(response.text)
      expect(response.status).to.equal(200);
      done();
    })
  })
})
