let product_Format = {
	_csrf:"NhEF5bSF-5e5b7aClW-awtF0DELdKdplGa1o",
	comment:"This product is gonna be sold. Bye to it!",
	delivery_method:"4",
	orders:[
		{
			isSubsidized:false, 
			lot_number:"1684/36564/564",
			product_id:"59b920aa59baae54d65d91c4",
			product_type:"3",
			qtty_cart:"45",
			status:"1",
			supplier_id:"5af1a04ea9b3263e5cd5d62c",
			supplier_name:"5af1a04ea9b3263e5cd5d62c"
		}
	],
	wh_id:"5af1a386a9b3263e5cd5d62e"	
}

let supplier_Credentials={email:"prucletiki@ququb.com",
							password:"prucletiki@ququb.com"};


//Require the dev-dependencies
let chai = require('chai');
var agent = require('superagent').agent();
let should = chai.should();
let expect = chai.expect;
let assert = chai.assert;
let config= require ("./config");

var clone = (a)=> {
	return JSON.parse(JSON.stringify(a));
}

let URL = config.URL;
let stock_before_stock_out;
let stock_after_stock_out;

/**************************
 * Testing stock out
 * Transfer btn Warehouses
 * and Sale of parts of 
 * a warehouse.
 **************************/

describe('Testing Stock Out options for the supplier', () => {
	
	before(function(done) {
		agent.post(URL+'/user.signin')
		.send(supplier_Credentials)
		.end(function(err, response) {
			expect(response.statusCode).to.equal(200);
			done();
		})
	});

	it("it should let a supplier see the stock before stock out", (done) => {
		agent.get(URL+'/product.stock.info/5af1a04ea9b3263e5cd5d62c/'+ 
			product_Format.wh_id + '/' + product_Format.orders[0].product_id)
		.end((err, response) => {
			let _data = JSON.parse(response.text);
			let show = _data.find(function(element) {
				return element._id.lot_number === 
									product_Format.orders[0].lot_number;
			});
			stock_before_stock_out = show.number_items * show.unit_qtty;
			expect(response.status).to.equal(200);
			done();
		});
	});

	it("it should let a supplier make a stock out", (done)=> {
		agent.post(URL+'/warehouse.OUT/4')
		.send(product_Format)
		.end((err, response) => {
			expect(response.status).to.equal(200);
			done();
		});
	});

	it("The stock removed should be equal to the difference in stock before and after", (done) => {
		agent.get(URL+'/product.stock.info/5af1a04ea9b3263e5cd5d62c/'+ 
			product_Format.wh_id + '/' + product_Format.orders[0].product_id)
		.end((err, response) => {
			let _data = JSON.parse(response.text);
			let show = _data.find(function(element) {
				return element._id.lot_number === 
									product_Format.orders[0].lot_number;
			});

			stock_after_stock_out = show.number_items * show.unit_qtty;
			
			expect(stock_before_stock_out - stock_after_stock_out)
					.to.equal(parseInt(product_Format.orders[0].qtty_cart));
			done();
		});
	});


	// it("it should not let a supplier make a stock out with empty _csrf", (done)=> {
	// 	newProduct = clone(product_Format);
	// 	newProduct._csrf = "";
	// 	agent.post(URL+'/warehouse.OUT/4')
	// 	.send(newProduct)
	// 	.end((err, response) => {
	// 		expect(response.statusCode).to.equal(400);
	// 		done();
	// 	});
	// });

	// it("it should not let a supplier make a stock out with invalid _csrf ", (done)=> {
	// 	newProduct = clone(product_Format);
	// 	newProduct._csrf = "[*$]";
	// 	agent.post(URL+'/warehouse.OUT/4')
	// 	.send(newProduct)
	// 	.end((err, response) => {
	// 		expect(response.statusCode).to.equal(400);
	// 		done();
	// 	});
	// });

	it("it should not let a supplier make a stock out with empty delivery_method", (done)=> {
		newProduct = clone(product_Format);
		newProduct.delivery_method = "";
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});

	it("it should not let a supplier make a stock out with invalid delivery_method", (done)=> {
		newProduct = clone(product_Format);
		newProduct.delivery_method = "432";
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});

	it("it should not let a supplier make a stock out with empty product isSubsidized input", (done)=> {
		newProduct = clone(product_Format);
		newProduct.orders[0].isSubsidized = "";
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});

	it("it should not let a supplier make a stock out with invalid product isSubsidized input", (done)=> {
		newProduct = clone(product_Format);
		newProduct.orders[0].isSubsidized = 123;
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});

	it("it should not let a supplier make a stock out with empty product lot_numbers", (done)=> {
		newProduct = clone(product_Format);
		newProduct.orders[0].lot_number = "";
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});

	it("it should not let a supplier make a stock out with invalid product lot_number", (done)=> {
		newProduct = clone(product_Format);
		newProduct.orders[0].lot_number = "safdl;dfsd";
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});

	it("it should not let a supplier make a stock out with empty product_id", (done)=> {
		newProduct = clone(product_Format);
		newProduct.orders[0].product_id = "";
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});

	it("it should not let a supplier make a stock out with invalid product_id", (done)=> {
		newProduct = clone(product_Format);
		newProduct.orders[0].product_id = "jkdsad;fdalkjsadflkj";
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});

	it("it should not let a supplier make a stock out with empty product_name", (done)=> {
		newProduct = clone(product_Format);
		newProduct.orders[0].product_name = "";
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});

	it("it should not let a supplier make a stock out with invalid product_name", (done)=> {
		newProduct = clone(product_Format);
		newProduct.orders[0].product_name = "asdflkj";
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});

	it("it should not let a supplier make a stock out with empty product_type", (done)=> {
		newProduct = clone(product_Format);
		newProduct.orders[0].product_type = "";
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});

	it("it should not let a supplier make a stock out with invalid product_type", (done)=> {
		newProduct = clone(product_Format);
		newProduct.orders[0].product_type = "asdflkj";
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});

	it("it should not let a supplier make a stock out with empty qtty_cart", (done)=> {
		newProduct = clone(product_Format);
		newProduct.orders[0].qtty_cart = "";
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});

	it("it should not let a supplier make a stock out with invalid qtty_cart", (done)=> {
		newProduct = clone(product_Format);
		newProduct.orders[0].qtty_cart = "asdflkj";
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});

	it("it should not let a supplier make a stock out with empty status", (done)=> {
		newProduct = clone(product_Format);
		newProduct.orders[0].status = "";
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});

	it("it should not let a supplier make a stock out with invalid status", (done)=> {
		newProduct = clone(product_Format);
		newProduct.orders[0].status = "432";
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});

	it("it should not let a supplier make a stock out with empty supplier_id", (done)=> {
		newProduct = clone(product_Format);
		newProduct.orders[0].supplier_id = "";
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});

	it("it should not let a supplier make a stock out with invalid supplier_id", (done)=> {
		newProduct = clone(product_Format);
		newProduct.orders[0].supplier_id = ";lafdjafsdal;kjddsfalkjfdsalkjsdlkfj;lkjdf";
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});

	it("it should not let a supplier make a stock out with empty supplier_name", (done)=> {
		newProduct = clone(product_Format);
		newProduct.orders[0].supplier_name = "";
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});

	it("it should not let a supplier make a stock out with invalid supplier_name", (done)=> {
		newProduct = clone(product_Format);
		newProduct.orders[0].supplier_name = "sdfalkj;a;lkjsdafjlkdsajflkjdfsdflk";
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});

	it("it should not let a supplier make a stock out with empty wh_id", (done)=> {
		newProduct = clone(product_Format);
		newProduct.orders[0].wh_id = "";
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});

	it("it should not let a supplier make a stock out with invalid wh_id", (done)=> {
		newProduct = clone(product_Format);
		newProduct.orders[0].wh_id = "chick-fil-a";
		agent.post(URL+'/warehouse.OUT/4')
		.send(newProduct)
		.end((err, response) => {
			expect(response.statusCode).to.equal(400);
			done();
		});
	});
});

