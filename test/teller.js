// order_format
let orderFormat={
  amount:96900,
authorization_token:"$#4382fd3381557e2M@57f72da7de61dc60F0O9b150aaacee6fc2bc164ed5be319",
bankSlip:"ecwce56",
isValid:true,
order_id:"5b30aed182b69651eefb17dc",
transaction_ref:"6548951",
when:"2018-06-25T09:05:49.462Z"
}
//credentials
let Teller_credentials={email:"beruihuchot@2anome.com",password:"beruihuchot@2anome.com"}
//Require the dev-dependencies
let chai = require('chai');
var agent = require('superagent').agent();
let should = chai.should();
let expect = chai.expect;
let assert = chai.assert;
let newOrder;
var clone =(a)=>{
  return JSON.parse(JSON.stringify(a));
}
let URL="http://192.168.0.46:8000"
describe('testing what a teller can do',()=>{
  before(function(done){
    // login as a teller
	  agent.post(URL+'/user.signin')
	    .send(Teller_credentials)
	    .end(function(err, response){
	      expect(response.statusCode).to.equal(200);
	      done();
	    });
	});
  it('it should not let a teller make an operation without an authorization_token',(done)=>{
    // sending order_format without authorization_token
    newOrder=clone(orderFormat);
    newOrder.authorization_token=undefined;
    agent.post(URL+'/order.payment.confirmation')
    .send(newOrder)
    .end(function(err,response){
      expect(response.status).to.equal(400)
      done();
    })
  })
  it('it should not let a teller make an operation with an invalid authorization_token',(done)=>{
    // sending order_format with an invalid authorization_token
    newOrder=clone(orderFormat);
    newOrder.authorization_token="$#4382fd3381557e2M@57f72da7de61dc60F0O9b150aaacee6fc2bc164ed549082nm";
    agent.post(URL+'/order.payment.confirmation')
    .send(newOrder)
    .end(function(err,response){
      expect(response.status).to.equal(400)
      done();
    })
  })
  it('it should not let a teller make an operation with an empty bankSlip',(done)=>{
    // sending order_format with an empty bankSlip
    newOrder=clone(orderFormat);
    newOrder.bankSlip=undefined;
    agent.post(URL+'/order.payment.confirmation')
    .send(newOrder)
    .end(function(err,response){
      expect(response.status).to.equal(400)
      done();
    })
  })
  it('it should not let a teller make an operation with an empty order_id',(done)=>{
    // sending order_format with an empty order_id
    newOrder=clone(orderFormat);
    newOrder.order_id=undefined;
    agent.post(URL+'/order.payment.confirmation')
    .send(newOrder)
    .end(function(err,response){
      expect(response.status).to.equal(400)
      done();
    })
  })
  it('it should not let a teller make an operation with an invalid order_id',(done)=>{
    // sending order_format with an invalid order_id
    newOrder=clone(orderFormat);
    newOrder.order_id="5b30aed182b69651eefv1d7sc";
    agent.post(URL+'/order.payment.confirmation')
    .send(newOrder)
    .end(function(err,response){
      expect(response.status).to.equal(400)
      done();
    })
  })
  it('it should not let a teller make an operation without the amount',(done)=>{
    //sending order_format without the amount
    newOrder=clone(orderFormat);
    newOrder.amount=undefined;
    agent.post(URL+'/order.payment.confirmation')
    .send(newOrder)
    .end(function(err,response){
      expect(response.status).to.equal(400)
      done();
    })
  })
})
