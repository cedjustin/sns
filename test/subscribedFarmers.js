//Require the dev-dependencies
let chai = require('chai');
var agent = require('superagent').agent();
let should = chai.should();
let expect = chai.expect;
let assert = chai.assert;
let config= require ("./config");
let AD_Credentials ={email:"tunacikone@hvzoi.com",password:"tunacikone@hvzoi.com"},
  SUPPLIER_Credentials ={email:"prucletiki@ququb.com",password:"prucletiki@ququb.com"},
  Distributor_credentials={email:"bilihuchot@2anom.com",password:"bilihuchot@2anom.com"},
  Teller_credentials={email:"beruihuchot@2anome.com",password:"beruihuchot@2anome.com"},
  RAB_credentials={email:"egide.gatari@rab.gov.rw",password:"b88692447d933095a95515c0eb0f7f0382acc36ec4c894057db1379e6b18e51ba"},
  SA_credentials={email:"jeandamantaka@yahoo.com",password:"b88692447d933095a95515c0eb0f7f0382acc36ec4c894057db1379e6b18e51ba"},  
  DistrictA_credentials={email:"tgatoya@gmail.com",password:"b88692447d933095a95515c0eb0f7f0382acc36ec4c894057db1379e6b18e51ba"}  
  let URL =config.URL;
  describe("Tests to check who can see the subscribed farmers",()=>{
      //these tests should work
    before((done)=>{
        agent.post(URL+"/user.signin")
        .send(AD_Credentials)
        .end((err,Response)=>{
            expect(Response.statusCode).to.equal(200);
            done();
        })
    })
    it("should allow the agro_dealer to view the list of subscribed farmers",(done)=>{
        agent.get(URL+"/warehouse.subscription.list/5af022b6389c7231fb403312")
        .end((err,Response)=>{
        //console.log(Response.text)
        expect(Response.status).to.equal(200);
        done();
        })
    })
    //these test should not work
    before((done)=>{
        agent.post(URL+"/user.signin")
        .send(Distributor_credentials)
        .end((err,Response)=>{
            expect(Response.statusCode).to.equal(200);
            done();
        })
    })
    it("should not allow the distributor to view the list of subscribed farmers",(done)=>{
        agent.get(URL+"/warehouse.subscription.list/5af022b6389c7231fb403312")
        .end((err,Response)=>{
        //console.log(Response.text)
        expect(Response.status).to.equal(400);
        done();
        })
    })
    before((done)=>{
        agent.post(URL+"/user.signin")
        .send(SUPPLIER_Credentials)
        .end((err,Response)=>{
            expect(Response.statusCode).to.equal(200);
            done();
        })
    })
    it("should not allow the supplier to view the list of subscribed farmers",(done)=>{
        agent.get(URL+"/warehouse.subscription.list/5af022b6389c7231fb403312")
        .end((err,Response)=>{
        //console.log(Response.text)
        expect(Response.status).to.equal(400);
        done();
        })
    })
    // before((done)=>{
    //     agent.post(URL+"/user.signin")
    //     .send(SA_credentials)
    //     .end((err,Response)=>{
    //         expect(Response.statusCode).to.equal(200);
    //         done();
    //     })
    // })
    // it("should not allow the sector_Agronomist to view the list of subscribed farmers",(done)=>{
    //     agent.get(URL+"/warehouse.subscription.list/5af022b6389c7231fb403312")
    //     .end((err,Response)=>{
    //     //console.log(Response.text)
    //     expect(Response.status).to.equal(400);
    //     done();
    //     })
    // })
    // before((done)=>{
    //     agent.post(URL+"/user.signin")
    //     .send(RAB_credentials)
    //     .end((err,Response)=>{
    //         expect(Response.statusCode).to.equal(200);
    //         done();
    //     })
    // })
    // it("should not allow a RAB_agent to view the list of subscribed farmers",(done)=>{
    //     agent.get(URL+"/warehouse.subscription.list/5af022b6389c7231fb403312")
    //     .end((err,Response)=>{
    //     //console.log(Response.text)
    //     expect(Response.status).to.equal(400);
    //     done();
    //     })
    // })
})