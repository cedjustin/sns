//require dev dependencies
let chai = require('chai');
var agent = require('superagent').agent();
let async = require('async')
let config= require ("./config");
let should = chai.should();
let expect = chai.expect;
let assert = chai.assert;
//order_format
let orderFormat={
  "isSubsidized":true,
  "isSelfPicking":true,
  "originOrg_id":"5af1a04ea9b3263e5cd5d62c",
  "originWH_id":"5af1a386a9b3263e5cd5d62e",
  "offloadingMargin":"15",
  "driver":"fab",
  "plate":"rac672d",
  orders:[
  {
  "minMargin_AD":40,
  "names":"ks njoro ii",
  "qtty":199000.0000000005,
  "product_type":1,
  "product_id":"59bd5ba899573f31134b71b7",
  "quantity":700
  }
  ],
  "destOrg_id":"5af0207f5edecc3124c37ca6",
  "destWH_id":"5af022b6389c7231fb403312",
  "_csrf":"u8tm27QB-KqKD0r4P6TgGGQC6mtjOxRDyT0Q"
}
var clone =(a)=>{
  return JSON.parse(JSON.stringify(a));
}
//credentials
let AD_Credentials ={email:"tunacikone@hvzoi.com",password:"tunacikone@hvzoi.com"},
  SUPPLIER_Credentials ={email:"prucletiki@ququb.com",password:"prucletiki@ququb.com"},
  Distributor_credentials={email:"bilihuchot@2anom.com",password:"bilihuchot@2anom.com"},
  Teller_credentials={email:"beruihuchot@2anome.com",password:"beruihuchot@2anome.com"},
  SA_Credentials={email:"jeandamantaka@yahoo.com",password:"b88692447d933095a95515c0eb0f7f0382acc36ec4c894057db1379e6b18e51ba"}
let URL =config.URL;
let ObjectID=config.ObjectID;
let newOrder;

/*
    testing all possible cases that can happen when placing an order
    1. Agrodealer login
    2. Place an order which is not subsidized
    3.place an order which is subsidized
    4. Place an order which is subsidized and self_picking without driver's name
    5. Place an order which is subsidized and self_picking without plate_number
    6. Place an order which is subsidized and self_picking without originOrg_id
    7. Place an order which is subsidized and self_picking without originWH_id
    8. Place an order which is subsidized and self_picking without offloadingMargin
    9. Place an order which is subsidized and self_picking without orders
    10. Place an order which is subsidized and self_picking with orders without minMargin_AD
    11. Place an order which is subsidized and self_picking with orders without qtty
    12. Place an order which is subsidized and self_picking with orders without product_type
    13. Place an order which is subsidized and self_picking with orders without product_id
    14. Place an order which is subsidized and self_picking with orders without quantity
    15. Place an order which is subsidized and self_picking without destOrg_id
    16. Place an order which is subsidized and self_picking without destOrg_id
    17. Place an order which is subsidized and self_picking without destWH_id
    18. supplier login
    19. supplier places an order which is not subsidized
    20.  supplier places an order which is subsidized
    21. distributor login
    22. distributor places an order which is not subsidized
    23. sector_agronomist places an order which is not subsidized
    24. sector_agronomist places an order which is subsidized
*/

describe("testing all possible cases that can happen when placing an order",()=>{
    before((done)=>{//1. Agrodealer login
        agent.post(URL+'/user.signin')
          .send(AD_Credentials)
          .end(function(err, response){
            expect(response.statusCode).to.equal(200);
            done();
          });
      });
    // 2. Place an order which is not subsidized
    it("it should let an agroDealer make an order which is not subsidized",(done)=>{
    newOrder=clone(orderFormat);
    newOrder.isSubsidized=false;
    agent.post(URL+'/order.new')
    .send(newOrder)
    .end((err,response)=>{
        expect(response.status).to.equal(200);
        done();
        })
    });
    //3.place an order which is subsidized
    it("it should let an agroDealer make an order which is subsidized",(done)=>{
    agent.post(URL+'/order.new')
    .send(newOrder)
    .end((err,response)=>{
        expect(response.status).to.equal(200);
        done();
        })
    });
    //4. Place an order which is subsidized and self_picking without driver's name
    it("it should not let an agroDealer make an order which is subsidized nd self_picking without driver's name",(done)=>{
        newOrder=clone(orderFormat)
        newOrder.driver=undefined;
        agent.post(URL+'/order.new')
        .send(newOrder)
        .end((err,response)=>{
            expect(response.status).to.equal(400);
            done();
            })
    });
    //5. Place an order which is subsidized and self_picking without plate_number
    it("it should not let an agroDealer make an order which is subsidized nd self_picking without plate_number",(done)=>{
        newOrder=clone(orderFormat)
        newOrder.plate=undefined;
        agent.post(URL+'/order.new')
        .send(newOrder)
        .end((err,response)=>{
            expect(response.status).to.equal(400);
            done();
            })
    });
    //6. Place an order which is subsidized and self_picking without originOrg_id
    it("it should not let an agroDealer make an order which is subsidized nd self_picking without originOrg_id",(done)=>{
        newOrder=clone(orderFormat)
        newOrder.originOrg_id=undefined;
        agent.post(URL+'/order.new')
        .send(newOrder)
        .end((err,response)=>{
            expect(response.status).to.equal(400);
            done();
            })
    });
    //7. Place an order which is subsidized and self_picking without originWH_id
    it("it should not let an agroDealer make an order which is subsidized nd self_picking without originWH_id",(done)=>{
        newOrder=clone(orderFormat)
        newOrder.originOrg_id=undefined;
        agent.post(URL+'/order.new')
        .send(newOrder)
        .end((err,response)=>{
            expect(response.status).to.equal(400);
            done();
            })
    });
    //8. Place an order which is subsidized and self_picking without offloadingMargin
    it("it should not let an agroDealer make an order which is subsidized nd self_picking without offloadingMargin",(done)=>{
        newOrder=clone(orderFormat)
        newOrder.offloadingMargin=undefined;
        agent.post(URL+'/order.new')
        .send(newOrder)
        .end((err,response)=>{
            expect(response.status).to.equal(400);
            done();
            })
    });
    //9. Place an order which is subsidized and self_picking without orders
    it("it should not let an agroDealer make an order which is subsidized nd self_picking without orders",(done)=>{
        newOrder=clone(orderFormat)
        newOrder.orders=undefined;
        agent.post(URL+'/order.new')
        .send(newOrder)
        .end((err,response)=>{
            expect(response.status).to.equal(400);
            done();
            })
    });
    //10. Place an order which is subsidized and self_picking with orders without minMargin_AD
    it("it should not let an agroDealer make an order which is subsidized nd self_picking without minMargin_AD",(done)=>{
        newOrder=clone(orderFormat)
        newOrder.orders[0].minMargin_AD=undefined;
        agent.post(URL+'/order.new')
        .send(newOrder)
        .end((err,response)=>{
            expect(response.status).to.equal(400);
            done();
            })
    });
    //11. Place an order which is subsidized and self_picking with orders without qtty
    it("it should not let an agroDealer make an order which is subsidized nd self_picking without qtty",(done)=>{
        newOrder=clone(orderFormat)
        newOrder.orders[0].qtty=undefined;
        agent.post(URL+'/order.new')
        .send(newOrder)
        .end((err,response)=>{
            expect(response.status).to.equal(400);
            done();
            })
    });
    //12. Place an order which is subsidized and self_picking with orders without product_type
    it("it should not let an agroDealer make an order which is subsidized nd self_picking without product_type",(done)=>{
        newOrder=clone(orderFormat)
        newOrder.orders[0].product_type=undefined;
        agent.post(URL+'/order.new')
        .send(newOrder)
        .end((err,response)=>{
            expect(response.status).to.equal(400);
            done();
            })
    });
    //13. Place an order which is subsidized and self_picking with orders without product_id
    it("it should not let an agroDealer make an order which is subsidized nd self_picking without product_id",(done)=>{
        newOrder=clone(orderFormat)
        newOrder.orders[0].product_id=undefined;
        agent.post(URL+'/order.new')
        .send(newOrder)
        .end((err,response)=>{
            expect(response.status).to.equal(400);
            done();
            })
    });
    //14. Place an order which is subsidized and self_picking with orders without quantity
    it("it should not let an agroDealer make an order which is subsidized nd self_picking without quantity",(done)=>{
        newOrder=clone(orderFormat)
        newOrder.orders[0].quantity=undefined;
        agent.post(URL+'/order.new')
        .send(newOrder)
        .end((err,response)=>{
            expect(response.status).to.equal(400);
            done();
            })
    });
    //15. Place an order which is subsidized and self_picking without destOrg_id
    it("it should not let an agroDealer make an order which is subsidized and self_picking without destOrg_id",(done)=>{
        newOrder=clone(orderFormat)
        newOrder.destOrg_id=undefined;
        agent.post(URL+'/order.new')
        .send(newOrder)
        .end((err,response)=>{
            expect(response.status).to.equal(400);
            done();
            })
    });
    //16. Place an order which is subsidized and self_picking without destOrg_id
    it("it should not let an agroDealer make an order which is subsidized and self_picking without destOrg_id",(done)=>{
        newOrder=clone(orderFormat)
        newOrder.destOrg_id=undefined;
        agent.post(URL+'/order.new')
        .send(newOrder)
        .end((err,response)=>{
            expect(response.status).to.equal(400);
            done();
            })
    });
    //17. Place an order which is subsidized and self_picking without destWH_id
    it("it should not let an agroDealer make an order which is subsidized and self_picking without destWH_id",(done)=>{
        newOrder=clone(orderFormat)
        newOrder.destWH_id=undefined;
        agent.post(URL+'/order.new')
        .send(newOrder)
        .end((err,response)=>{
            expect(response.status).to.equal(400);
            done();
            })
    });

    before((done)=>{//18. supplier login
        agent.post(URL+'/user.signin')
          .send(SUPPLIER_Credentials)
          .end(function(err, response){
            expect(response.statusCode).to.equal(200);
            done();
          });
      });
    // 19. supplier places an order which is not subsidized
    it("it should not let supplier make an order which is not subsidized",(done)=>{
    newOrder=clone(orderFormat);
    newOrder.isSubsidized=false;
    agent.post(URL+'/order.new')
    .send(newOrder)
    .end((err,response)=>{
        expect(response.status).to.equal(200);
        done();
        })
    });
    //20.  supplier places an order which is subsidized
    it("it should not let a supplier make an order which is subsidized",(done)=>{
    agent.post(URL+'/order.new')
    .send(newOrder)
    .end((err,response)=>{
        expect(response.status).to.equal(200);
        done();
        })
    });
    before((done)=>{//21. distributor login
        agent.post(URL+'/user.signin')
          .send(Distributor_credentials)
          .end(function(err, response){
            expect(response.statusCode).to.equal(200);
            done();
          });
      });
    // 22. distributor places an order which is not subsidized
    it("it should not let a distributor make an order which is not subsidized",(done)=>{
    newOrder=clone(orderFormat);
    newOrder.isSubsidized=false;
    agent.post(URL+'/order.new')
    .send(newOrder)
    .end((err,response)=>{
        expect(response.status).to.equal(200);
        done();
        })
    });
    //21.  supplier places an order which is subsidized
    it("it should not let a distributor make an order which is subsidized",(done)=>{
    agent.post(URL+'/order.new')
    .send(newOrder)
    .end((err,response)=>{
        expect(response.status).to.equal(200);
        done();
        })
    });
    // before((done)=>{//22. sector_agronomist login
    //     agent.post(URL+'/user.signin')
    //       .send(SA_Credentials)
    //       .end(function(err, response){
    //         expect(response.statusCode).to.equal(200);
    //         done();
    //       });
    //   });
    // // 23. sector_agronomist places an order which is not subsidized
    // it("it should not let a sector_agronomist make an order which is not subsidized",(done)=>{
    // newOrder=clone(orderFormat);
    // newOrder.isSubsidized=false;
    // agent.post(URL+'/order.new')
    // .send(newOrder)
    // .end((err,response)=>{
    //     expect(response.status).to.equal(200);
    //     done();
    //     })
    // });
    // //24. sector_agronomist places an order which is subsidized
    // it("it should not let a sector_agronomist make an order which is subsidized",(done)=>{
    // agent.post(URL+'/order.new')
    // .send(newOrder)
    // .end((err,response)=>{
    //     expect(response.status).to.equal(200);
    //     done();
    //     })
    // });
})
