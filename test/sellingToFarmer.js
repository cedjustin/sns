//require dev dependencies
let chai = require('chai');
var agent = require('superagent').agent();
let async = require('async')
let config= require ("./config");
let should = chai.should();
let expect = chai.expect;
let assert = chai.assert;
let farmer_id="aa04247",qtty="55";
let selected_farmer,selected_fert,qtty_in_store_before=0,qtty_in_store_after=0;
//order_format
let order_format={
  "_csrf":"r2Yfj8OL-Xk4z_VGgKzpVodd5hP-f0ZxG-3s",
  "wh_id":"5af022b6389c7231fb403312",
  "regNo":farmer_id,
  "isSubsidized":true,
  "orders":[
      {
      "lot_number":"561/6354",
      "supplier_name":"5af1a04ea9b3263e5cd5d62c",
      "supplier_id":"5af1a04ea9b3263e5cd5d62c",
      "product_name":"DAP",
      "product_id":"59b8fa3996a2b87300d9760c",
      "product_type":3,
      "status":1,
      "isSubsidized":true,
      "qtty_cart":qtty
      }
    ]
  };
let new_order;
let sub_farmers;
var clone =(a)=>{
  return JSON.parse(JSON.stringify(a));
}
//credentials
let AD_Credentials ={email:"tunacikone@hvzoi.com",password:"tunacikone@hvzoi.com"},
  SUPPLIER_Credentials ={email:"prucletiki@ququb.com",password:"prucletiki@ququb.com"},
  Distributor_credentials={email:"bilihuchot@2anom.com",password:"bilihuchot@2anom.com"},
  Teller_credentials={email:"beruihuchot@2anome.com",password:"beruihuchot@2anome.com"},
  SA_Credentials={email:"jeandamantaka@yahoo.com",password:"b88692447d933095a95515c0eb0f7f0382acc36ec4c894057db1379e6b18e51ba"}
let URL =config.URL
describe("selling to farmer",()=>{
  before((done)=>{
    //signing as agro_dealer
    agent.post(URL+"/user.signin")
    .send(AD_Credentials)
    .end((err,response)=>{
      expect(response.statusCode).to.equal(200);
      done();
    })
  });
  it("should allow the agro_dealer to view the subscribed farmers",(done)=>{
    //viewing subscribed farmers as agro_dealer
    agent.get(URL+"/warehouse.subscription.list/5af022b6389c7231fb403312")
    .end((err,response)=>{
        sub_farmers=JSON.parse(response.text);
        farmer_info=sub_farmers.contracts;
        selected_farmer=farmer_info.find((element)=>{
          return element.used_by.regNumber===farmer_id;
        })
        // console.log(selected_farmer);
        expect(response.status).to.equal(200);
        done();
    });
  });
  it("should allow the agro_dealer to view the stock",(done)=>{
    //viewing all fertilizers in stock as agro_dealer
    agent.get(URL+"/warehouse.subscription.ferts.charts/5af022b6389c7231fb403312")
    .end((err,response)=>{
        ferts=JSON.parse(response.text);
        selected_fert=ferts.find((element)=>{
          return element.fert_id===selected_farmer.plantingFerts[0]._id._id;
        })
        new_order=clone(order_format);
        expect(response.status).to.equal(200);
    });
    //viewing all seeds in stock as agro_dealer
    agent.get(URL+"/warehouse.subscription.seeds.charts/5af022b6389c7231fb403312")
    .end((err,response)=>{
    expect(response.status).to.equal(200);
    done();
    })
})
it("should allow the agro_dealer to view more info on one product",(done)=>{
  //viewing information on a crop ordered by a farmer in stock as agro_dealer
  agent.get(URL+"/product.stock.info/5af0207f5edecc3124c37ca6/5af022b6389c7231fb403312/"+selected_fert.fert_id)
  .end((err,response)=>{
    store=JSON.parse(response.text);
    for (let count = 0; count < store.length; count++) {
      //counting the quantity in stock before sale
      qtty_in_store_before+=store[count].number_items*store[count].unit_qtty;
    }
    for(count=0;count<store.length;count++){
      //checking if the quantity in stock is less than the ordered quantity
      if(qtty_in_store_before<qtty){
        console.log("there's no enough qtty to sale");
        break;
      }
      else if(store[count].number_items*store[count].unit_qtty>=qtty){
        //creating a new_order based on what the farmer ordered
        new_order.orders[0].lot_number=store[count]._id.lot_number;
        new_order.orders[0].supplier_id=store[count]._id.supplier_id;
        new_order.orders[0].supplier_name=store[count]._id.supplier_id;
        new_order.orders[0].status=store[count]._id.status;
        new_order.isSubsidized=store[count]._id.isSubsidized;
        new_order.orders[0].isSubsidized=store[count]._id.isSubsidized;
        new_order.orders[0].product_type=store[count].product_type;
        new_order.orders[0].product_id=store[count]._id.product_id;
        new_order.orders[0].product_name=selected_fert.fert_name;
        // console.log(new_order);
        break;
      }
      else{
        console.log(store[count]);
      }
    }
    expect(response.status).to.equal(200);
    done();
  })
})
it("the agro_dealer should not be able to sell to farmer more than what the farmer is allowed to have",(done)=>{
  //selling to the farmer 55kgs
  agent.post(URL+"/order.new.sale")
  .send(new_order)
  .end((err,response)=>{
    console.log(response.text);
    expect(response.status).to.equal(400);
    done();
  })
})
it("the agro_dealer should be able to sell to farmer",(done)=>{
  //selling to the farmer 5kgs
  new_order.orders[0].qtty_cart="5";
  agent.post(URL+"/order.new.sale")
  .send(new_order)
  .end((err,response)=>{
    console.log(response.text);
    expect(response.status).to.equal(200);
    done();
  })
})
it("should allow the agro_dealer to view more info on one product after sale",(done)=>{
  //comparing the stock before and after sale
  agent.get(URL+"/product.stock.info/5af0207f5edecc3124c37ca6/5af022b6389c7231fb403312/"+selected_fert.fert_id)
  .end((err,response)=>{
    store=JSON.parse(response.text);
    for(count=0;count<store.length;count++){
      qtty_in_store_after+=store[count].number_items*store[count].unit_qtty;
    }
    // console.log(qtty_in_store_after);
    // console.log(qtty_in_store_before+""+qtty_in_store_after);
    expect(qtty_in_store_before).to.be.below(qtty_in_store_after);
    done();
    })
  })
})