let crop_format = {
	"names":{
		"en":"Polaroids",
		"kiny":"polaroyidi"
	},
	"plantingFerts":[{
		"_id":"59b920aa59baae54d65d91c4",
		"kgPerAre":"3",
		"type":"1"
	}],
	"relatedFerts": null,
	"type_num": 3,
	"weedingFerts":[{
		"_id":"59ba517d8d109e5ad25e4c79",
		"kgPerAre":"1.25",
		"type":"2"
	}]
}

//Require the dev-dependencies
let chai = require('chai');
var agent = require('superagent').agent();
let should = chai.should();
let expect = chai.expect;
let assert = chai.assert;
let newOrder;
let newProduct;
let config= require ("./config");

var clone =(a)=>{
return JSON.parse(JSON.stringify(a));
}

let URL =config.URL;

let AD_Credentials ={email:"tunacikone@hvzoi.com",password:"tunacikone@hvzoi.com"},
  SUPPLIER_Credentials ={email:"prucletiki@ququb.com",password:"prucletiki@ququb.com "},
  Distributor_credentials={email:"bilihuchot@2anom.com",password:"bilihuchot@2anom.com"},
  Teller_credentials={email:"beruihuchot@2anome.com",password:"beruihuchot@2anome.com"},
  RAB_credentials={email:"egide.gatari@rab.gov.rw",password:"b88692447d933095a95515c0eb0f7f0382acc36ec4c894057db1379e6b18e51ba"},
  SA_credentials={email:"jeandamantaka@yahoo.com",password:"b88692447d933095a95515c0eb0f7f0382acc36ec4c894057db1379e6b18e51ba"},  
  DistrictA_credentials={email:"tgatoya@gmail.com",password:"b88692447d933095a95515c0eb0f7f0382acc36ec4c894057db1379e6b18e51ba"};


describe("Testing the process to add a new crop", ()=>{
	before((done)=>{
		agent.post(URL+"/user.signin")
		.send(RAB_credentials)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		})
	});

	it("It should allow a RAB agent to add a new crop", (done)=>{
		agent.post(URL+"/crop.new")
		.send(crop_format)
		.end((err, response)=>{
			console.log(response.text)
			expect(response.statusCode).to.equal(200);
			done();
		})
	});

	it("It should not allow a RAB agent to add a new crop with empty names", (done)=>{
		let newCrop = clone(crop_format);
		newCrop.names = "";
		agent.post(URL+"/crop.new")
		.send(newCrop)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	});

	it("It should not allow a RAB agent to add a new crop with empty english names", (done)=>{
		let newCrop = clone(crop_format);
		newCrop.names.en = "";
		agent.post(URL+"/crop.new")
		.send(newCrop)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	});

	it("It should not allow a RAB agent to add a new crop with empty kiny names", (done)=>{
		let newCrop = clone(crop_format);
		newCrop.names.kiny = "";
		agent.post(URL+"/crop.new")
		.send(newCrop)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	});

	it("It should not allow a RAB agent to add a new crop with empty plantingFerts", (done)=>{
		let newCrop = clone(crop_format);
		newCrop.plantingFerts = "";
		agent.post(URL+"/crop.new")
		.send(newCrop)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	});

	it("It should not allow a RAB agent to add a new crop with empty weedingFerts", (done)=>{
		let newCrop = clone(crop_format);
		newCrop.weedingFerts = "";
		agent.post(URL+"/crop.new")
		.send(newCrop)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	});

	it("It should not allow a RAB agent to add a new crop with empty type_num", (done)=>{
		let newCrop = clone(crop_format);
		newCrop.type_num = "";
		agent.post(URL+"/crop.new")
		.send(newCrop)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	});

	it("It should not allow a RAB agent to add a new crop with empty relatedFerts", (done)=>{
		let newCrop = clone(crop_format);
		newCrop.relatedFerts = "";
		agent.post(URL+"/crop.new")
		.send(newCrop)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	});
})

describe("Agro_Dealer Permissions over new crops", ()=>{
	before((done)=>{
		agent.post(URL+"/user.signin")
		.send(AD_Credentials)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		})
	});

	it("It should not allow a Agro_Dealer to add a new crop", (done)=>{
		agent.post(URL+"/crop.new")
		.send(crop_format)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	});
})

// describe("SUPPLIER Permissions over new crops", ()=>{
// 	before((done)=>{
// 		agent.post(URL+"/user.signin")
// 		.send(SUPPLIER_Credentials)
// 		.end((err, response)=>{
// 			expect(response.statusCode).to.equal(200);
// 			done();
// 		})
// 	});

// 	it("It should not allow a SUPPLIER to add a new crop", (done)=>{
// 		agent.post(URL+"/crop.new")
// 		.send(crop_format)
// 		.end((err, response)=>{
//
// 			expect(response.statusCode).to.equal(400);
// 			done();
// 		})
// 	});
// })

describe("Distributor Permissions over new crops", ()=>{
	before((done)=>{
		agent.post(URL+"/user.signin")
		.send(Distributor_credentials)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		})
	});

	it("It should not allow a Distributor to add a new crop", (done)=>{
		agent.post(URL+"/crop.new")
		.send(crop_format)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	});
})

describe("Teller Permissions over new crops", ()=>{
	before((done)=>{
		agent.post(URL+"/user.signin")
		.send(Teller_credentials)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		})
	});

	it("It should not allow a Teller to add a new crop", (done)=>{
		agent.post(URL+"/crop.new")
		.send(crop_format)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	});
})

describe("Sector Agronomist Permissions over new crops", ()=>{
	before((done)=>{
		agent.post(URL+"/user.signin")
		.send(SA_credentials)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		})
	});

	it("It should not allow a Sector Agronomist to add a new crop", (done)=>{
		agent.post(URL+"/crop.new")
		.send(crop_format)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	});
})

describe("District Agronomist Permissions over new crops", ()=>{
	before((done)=>{
		agent.post(URL+"/user.signin")
		.send(DistrictA_credentials)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		})
	});

	it("It should not allow a District Agronomist to add a new crop", (done)=>{
		agent.post(URL+"/crop.new")
		.send(crop_format)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	});
})