//require dev dependencies
let chai = require('chai');
var agent = require('superagent').agent();
let async = require('async')
let config= require ("./config");
let should = chai.should();
let expect = chai.expect;
let assert = chai.assert;
let orderFormat={
    "isSubsidized":true,
    "isSelfPicking":true,
    "originOrg_id":"5af1a04ea9b3263e5cd5d62c",
    "originWH_id":"5af1a386a9b3263e5cd5d62e",
    "offloadingMargin":"15",
    "driver":"fab",
    "plate":"rac672d",
    "orders":[
      {
      "minMargin_AD":40,
      "names":"ks njoro ii",
      "qtty":199000.0000000005,
      "product_type":1,
      "product_id":"59bd5ba899573f31134b71b7",
      "quantity":700
      }
    ],
    "destOrg_id":"5af0207f5edecc3124c37ca6",
    "destWH_id":"5af022b6389c7231fb403312",
    "_csrf":"u8tm27QB-KqKD0r4P6TgGGQC6mtjOxRDyT0Q"
}
var clone =(a)=>{
  return JSON.parse(JSON.stringify(a));
}
let AD_Credentials ={email:"tunacikone@hvzoi.com",password:"tunacikone@hvzoi.com"},
  SUPPLIER_Credentials ={email:"prucletiki@ququb.com",password:"prucletiki@ququb.com"},
  Distributor_credentials={email:"bilihuchot@2anom.com",password:"bilihuchot@2anom.com"},
  Teller_credentials={email:"beruihuchot@2anome.com",password:"beruihuchot@2anome.com"},
  SA_Credentials={email:"jeandamantaka@yahoo.com",password:"b88692447d933095a95515c0eb0f7f0382acc36ec4c894057db1379e6b18e51ba"}
let URL =config.URL;
let ObjectID=config.ObjectID;
let order_Id,newOrder;
  /*
 case1:testing an order which is not subsidized and agroDealer will do the transport(selfPicking)
 case2:testing an order which is subsidized and agroDealer will do the transport(selfPicking)
 1. Agrodealer login
 2. Place an order and get the order_id
 3. Login as Distributor
 4. Distributor tries to approves an order
 5. Login as Agro_dealer
 6. Agro_dealer tries to approves an order
 7. Login as Sector_agronomist
 8. Sector_agronomist tries to approves an order 
 9. Login as Supplier
 10. Supplier tries to approves an order
 11. Agro_dealer login
 12. Agro_dealer places a subsidized and non_subsidized orders where selfPicking = false
 13. Distributor login
 14. Distributor tries to approve a subsidized order where self_picking = false and without providing the driver's name
 15. Distributor tries to approve an order where self_picking = false and without providing the driver's plate number
 16. Supplier login
 17. Supplier tries to approve a subsidized order where self_picking = false and without providing the driver's name
 18. Supplier tries to approve an order where self_picking = false and without providing the driver's plate number
*/

//case1:testing an order which is not subsidized and agroDealer will do the transport(selfPicking)
describe('Testing creation of subsidized order',()=>{
  before((done)=>{//1. Agrodealer login
    agent.post(URL+'/user.signin')
      .send(AD_Credentials)
      .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
      });
  });
  // 2. Place an order and get the order_id
  it("it should let an agroDealer make an order",(done)=>{
    newOrder=clone(orderFormat);
    newOrder.isSubsidized=false;
    agent.post(URL+'/order.new')
    .send(newOrder)
    .end((err,response)=>{
      order_Id=config.stripquotes(response.text)
      expect(response.status).to.equal(200);
      done();
    })
  });
})
describe('Testing approval of a non subsidized order',function(){
  before((done)=>{//3. Login as Distributor
    agent.post(URL+'/user.signin')
      .send(Distributor_credentials)
      .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
      });
  });
  it("it should not let a distributor approve an order which is not subsidized",(done)=>{//4. Distributor tries to approves an order which is not subsidized
    agent.put(URL+'/order.approve')
    .send({order_id:order_Id})
    .end((err,response)=>{
      expect(response.status).to.equal(400)
      done();
    })
  });
  before((done)=>{//5. Login as Agro_dealer
    agent.post(URL+'/user.signin')
      .send(AD_Credentials)
      .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
      });
  });
  it("it should not let an agro_dealer approve an order which is not subsidized",(done)=>{//6. Agro_dealer tries to approves an order which is not subsidized
    agent.put(URL+'/order.approve')
    .send({order_id:order_Id})
    .end((err,response)=>{
      expect(response.status).to.equal(400)
      done();
    })
  });
<<<<<<< HEAD
  // before((done)=>{//7. Login as Sector_agronomist
  //   agent.post(URL+'/user.signin')
  //     .send(SA_Credentials)
  //     .end(function(err, response){
  //       expect(response.statusCode).to.equal(200);
  //       done();
  //     });
  // });
  // it("it should not let a sector_agronomist approve an order which is not subsidized",(done)=>{//8. Sector tries to approves an order which is not subsidized
  //   agent.put(URL+'/order.approve')
  //   .send({order_id:order_Id})
  //   .end((err,response)=>{
  //     //console.log('>>>'+JSON.stringify(response.text,null,2))
  //     expect(response.status).to.equal(400)
  //     done();
  //   })
  // });
=======
  before((done)=>{//7. Login as Sector_agronomist
    agent.post(URL+'/user.signin')
      .send(SA_Credentials)
      .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
      });
  });
  it("it should not let a sector_agronomist approve an order which is not subsidized",(done)=>{//8. Sector tries to approves an order which is not subsidized
    agent.put(URL+'/order.approve')
    .send({order_id:order_Id})
    .end((err,response)=>{
      expect(response.status).to.equal(400)
      done();
    })
  });
>>>>>>> 79902d477d7595bd5d5daa6033b9f847eb35eea5
  before((done)=>{//9. Login as Supplier
    agent.post(URL+'/user.signin')
      .send(SUPPLIER_Credentials)
      .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
      });
  });
  it("it should let a supplier approve an order which is not subsidized",(done)=>{//10. Supplier tries to approves an order which is not subsidized
    agent.put(URL+'/order.approve')
    .send({order_id:order_Id})
    .end((err,response)=>{
      expect(response.status).to.equal(400)
      done();
    })
  });
})

//case2:testing an order which is subsidized and agroDealer will do the transport(selfPicking)
describe('Testing creation of a subsidized order',()=>{
  before((done)=>{//1. Agrodealer login
    agent.post(URL+'/user.signin')
      .send(AD_Credentials)
      .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
      });
  });
// 2. Place an order which is not subsidized
it("it should let an agroDealer make an order which is not subsidized",(done)=>{
  newOrder=clone(orderFormat);
  agent.post(URL+'/order.new')
  .send(newOrder)
  .end((err,response)=>{
    order_Id=config.stripquotes(response.text);
    expect(response.status).to.equal(200);
    done();
    })
  });
})
describe('Testing approval for a subsidized order',function(){
<<<<<<< HEAD
  // before((done)=>{//5. Login as Agro_dealer
  //   agent.post(URL+'/user.signin')
  //     .send(AD_Credentials)
  //     .end(function(err, response){
  //       expect(response.statusCode).to.equal(200);
  //       done();
  //     });
  // });
  // it("it should not let an agro_dealer approve an order which is subsidized",(done)=>{//6. Agro_dealer tries to approves an order which is subsidized
  //   agent.put(URL+'/order.approve')
  //   .send({order_id:order_Id})
  //   .end((err,response)=>{
  //     //console.log('>>>'+JSON.stringify(response.text))
  //     expect(response.status).to.equal(400)
  //     done();
  //   })
  // });
  // before((done)=>{//7. Login as Sector_agronomist
  //   agent.post(URL+'/user.signin')
  //     .send(SA_Credentials)
  //     .end(function(err, response){
  //       expect(response.statusCode).to.equal(200);
  //       done();
  //     });
  // });
  // it("it should not let a sector_agronomist approve an order which is subsidized",(done)=>{//8. Sector_agronomists tries to approves an order which is subsidized
  //   agent.put(URL+'/order.approve')
  //   .send({order_id:order_Id})
  //   .end((err,response)=>{
  //     //console.log('>>>'+JSON.stringify(response.text,null,2))
  //     expect(response.status).to.equal(400)
  //     done();
  //   })
  // });
  // before((done)=>{//7. Login as Supplier
  //   agent.post(URL+'/user.signin')
  //     .send(SUPPLIER_Credentials)
  //     .end(function(err, response){
  //       expect(response.statusCode).to.equal(200);
  //       done();
  //     });
  // });
  // it("it should not let a supplier approve an order which is subsidized",(done)=>{//8. Supplier tries to approves an order which is subsidized
  //   agent.put(URL+'/order.approve')
  //   .send({order_id:order_Id})
  //   .end((err,response)=>{
  //     console.log('>>>'+JSON.stringify(response.text,null,2))
  //     expect(response.status).to.equal(400)
  //     done();
  //   })
  // });
=======
  before((done)=>{//5. Login as Agro_dealer
    agent.post(URL+'/user.signin')
      .send(AD_Credentials)
      .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
      });
  });
  it("it should not let an agro_dealer approve an order which is subsidized",(done)=>{//6. Agro_dealer tries to approves an order which is subsidized
    agent.put(URL+'/order.approve')
    .send({order_id:order_Id})
    .end((err,response)=>{
      expect(response.status).to.equal(400)
      done();
    })
  });
  before((done)=>{//7. Login as Sector_agronomist
    agent.post(URL+'/user.signin')
      .send(SA_Credentials)
      .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
      });
  });
  it("it should not let a sector_agronomist approve an order which is subsidized",(done)=>{//8. Sector_agronomists tries to approves an order which is subsidized
    agent.put(URL+'/order.approve')
    .send({order_id:order_Id})
    .end((err,response)=>{
      expect(response.status).to.equal(400)
      done();
    })
  });
  before((done)=>{//7. Login as Supplier
    agent.post(URL+'/user.signin')
      .send(SUPPLIER_Credentials)
      .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
      });
  });
  it("it should let a supplier approve an order which is subsidized",(done)=>{//8. Supplier tries to approves an order which is subsidized
    agent.put(URL+'/order.approve')
    .send({order_id:order_Id})
    .end((err,response)=>{
      console.log('>>>'+JSON.stringify(response.text,null,2))
      expect(response.status).to.equal(400)
      done();
    })
  });
>>>>>>> 79902d477d7595bd5d5daa6033b9f847eb35eea5
  before((done)=>{//3. Login as Distributor
    agent.post(URL+'/user.signin')
      .send(Distributor_credentials)
      .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
      });
  });
  it("it should let a distributor approve an order which is subsidized",(done)=>{//4. Distributor tries to approves an order which is subsidized
    agent.put(URL+'/order.approve')
    .send({order_id:order_Id})
    .end((err,response)=>{
      console.log('>>>'+JSON.stringify(response.text))
      expect(response.status).to.equal(200)
      done();
    })
  });
})
describe("testing approval when self_picking=false",()=>{
  before((done)=>{
    agent.post(URL+'/user.signin')
      .send(AD_Credentials)
      .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
      });
  });
  it("it should let an agroDealer make an order which is not subsidized",(done)=>{
    order_Id=null;
    newOrder=clone(orderFormat);
    newOrder.isSelfPicking=false
    agent.post(URL+'/order.new')
    .send(newOrder)
    .end((err,response)=>{
      order_Id=config.stripquotes(response.text)
      expect(response.status).to.equal(200);
      done();
      })
  });
})
describe("<<<",()=>{
  before((done)=>{//13. Distributor login
    agent.post(URL+"/user.signin")
    .send(Distributor_credentials)
    .end((err,response)=>{
      expect(response.statusCode).to.equal(200)
      done()
    })
  })
<<<<<<< HEAD
  // it("it should not let a distributor approve an order without providing the driver's name",(done)=>{//14. Distributor tries to approve a subsidized order where self_picking = false and without providing the driver's name
  //   agent.put(URL+"/order.approve")
  //   .send({
  //     "order_id":order_Id,
  //     "driver":{
  //     "name":undefined,
  //     "plate":"rac457d"
  //     }
  //     })
  //   .end((err,response)=>{
  //     //console.log(response.text)
  //     expect(response.status).to.equal(400);
  //     done();
  //   })
  // })
  // it("it should not let a distributor approve an order without providing the driver's plate number",(done)=>{//15. Distributor tries to approve an order where self_picking = false and without providing the driver's plate number
  //   agent.put(URL+"/order.approve")
  //   .send({
  //     "order_id":order_Id,
  //     "driver":{
  //     "name":"fab",
  //     "plate":undefined
  //     }
  //     })
  //   .end((err,response)=>{
  //     expect(response.status).to.equal(400);
  //     done();
  //   })
  // })
=======
  it("it should not let a distributor approve an order without providing the driver's name",(done)=>{//14. Distributor tries to approve a subsidized order where self_picking = false and without providing the driver's name
    agent.put(URL+"/order.approve")
    .send({
      "order_id":order_Id,
      "driver":{
      "name":undefined,
      "plate":"rac457d"
      }
      })
    .end((err,response)=>{
      expect(response.status).to.equal(400);
      done();
    })
  })
  it("it should not let a distributor approve an order without providing the driver's plate number",(done)=>{//15. Distributor tries to approve an order where self_picking = false and without providing the driver's plate number
    agent.put(URL+"/order.approve")
    .send({
      "order_id":order_Id,
      "driver":{
      "name":"fab",
      "plate":undefined
      }
      })
    .end((err,response)=>{
      expect(response.status).to.equal(400);
      done();
    })
  })
>>>>>>> 79902d477d7595bd5d5daa6033b9f847eb35eea5
  it("it should let a distributor approve an order ",(done)=>{
    agent.put(URL+"/order.approve")
    .send({
      "order_id":order_Id,
      "driver":{
      "name":"fab",
      "plate":"RAC892d"
      }
      })
    .end((err,response)=>{
      console.log(JSON.stringify(response.text,null,2))
      expect(response.status).to.equal(200);
      done();
    })
  })
})
describe(">>>",()=>{
  before((done)=>{
    agent.post(URL+"/user.signin")
    .send(AD_Credentials)
    .end((err,response)=>{
      expect(response.statusCode).to.equal(200)
      done()
    })
  })
  it("it should let an agro_dealer make an order when self_Picking=false and isSubsidized=false",(done)=>{
    order_Id=null;
    newOrder=clone(orderFormat);
    newOrder.isSelfPicking=false,
    newOrder.isSubsidized=false
    agent.post(URL+"/order.new")
    .send(newOrder)
    .end((err,response)=>{
      order_Id=config.stripquotes(response.text)
      console.log(response.request);
      console.log(order_Id);
      expect(response.status).to.equal(200);
      done()
    })  
  })
  before((done)=>{
    agent.post(URL+"/user.signin")
    .send(SUPPLIER_Credentials)
    .end((err,response)=>{
      expect(response.statusCode).to.equal(200)
      done()
    })
  })
  it("it should not let a supplier approve an order without providing the driver's name",(done)=>{
    agent.put(URL+"/order.approve")
    .send({
      "order_id":order_Id,
      "driver":{
      "name":undefined,
      "plate":"RAC122d"
      }
      })
    .end((err,response)=>{
      expect(response.status).to.equal(400);
      done()
    })
  })
  it("it should not let a supplier approve an order without providing the driver's plate number",(done)=>{
    agent.put(URL+"/order.approve")
    .send({
      "order_id":order_Id,
      "driver":{
      "name":"fab",
      "plate":undefined
      }
      })
    .end((err,response)=>{
      expect(response.status).to.equal(400);
      done()
    })
  })
  it("it should let a supplier approve an order",(done)=>{
    agent.put(URL+"/order.approve")
    .send({
      "order_id":order_Id,
      "driver":{
      "name":"fab",
      "plate":"rac896d"
      }
      })
    .end((err,response)=>{
      console.log(JSON.stringify(order_Id+response.text,null,2))
      expect(response.status).to.equal(200);
      done()
    })
  })
})