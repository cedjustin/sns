//require dev dependencies
let chai = require('chai');
var agent = require('superagent').agent();
let async = require('async')
let config= require ("./config");
let should = chai.should();
let expect = chai.expect;
let assert = chai.assert;
let orderFormat={
  "isSubsidized":true,
  "isSelfPicking":true,
  "originOrg_id":"5af1a04ea9b3263e5cd5d62c",
  "originWH_id":"5af1a386a9b3263e5cd5d62e",
  "offloadingMargin":"15",
  "driver":"fab",
  "plate":"rac672d",
  "orders":[
  {
  "minMargin_AD":40,
  "names":"ks njoro ii",
  "qtty":199000.0000000005,
  "product_type":1,
  "product_id":"59bd5ba899573f31134b71b7",
  "quantity":700
  }
  ],
  "destOrg_id":"5af0207f5edecc3124c37ca6",
  "destWH_id":"5af022b6389c7231fb403312",
  "_csrf":"u8tm27QB-KqKD0r4P6TgGGQC6mtjOxRDyT0Q"
}
var clone =(a)=>{
  return JSON.parse(JSON.stringify(a));
}
let AD_Credentials ={email:"tunacikone@hvzoi.com",password:"tunacikone@hvzoi.com"},
  SUPPLIER_Credentials ={email:"prucletiki@ququb.com",password:"prucletiki@ququb.com"},
  Distributor_credentials={email:"bilihuchot@2anom.com",password:"bilihuchot@2anom.com"},
  Teller_credentials={email:"beruihuchot@2anome.com",password:"beruihuchot@2anome.com"},
  SA_Credentials={email:"jeandamantaka@yahoo.com",password:"b88692447d933095a95515c0eb0f7f0382acc36ec4c894057db1379e6b18e51ba"}
let URL =config.URL;
let order_Id,totalPrice,newOrder;
let order_id;
let approved_order={
  amount:totalPrice,
  bankSlip:config.genUID(12),
  transaction_ref:config.genUID(10),
  when:new Date(),
  isValid:true,
  authorization_token:config.AUTH_TOKEN,
  order_id
}
describe('Testing creation',()=>{
  before((done)=>{
    agent.post(URL+'/user.signin')
      .send(AD_Credentials)
      .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
      });
  });
  it("it should let an agroDealer make an order",(done)=>{
    agent.post(URL+'/order.new')
    .send(orderFormat)
    .end((err,response)=>{
      order_Id=config.stripquotes(response.text)
      console.log(">>>>>>"+" >> "+order_Id)
      expect(response.status).to.equal(200);
      done();
    })
  });
})
describe('Testing approval',function(){
  before((done)=>{
    agent.post(URL+'/user.signin')
      .send(Distributor_credentials)
      .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
      });
  });
  it("it should let a distributor approve an order",(done)=>{
    agent.put(URL+'/order.approve')
    .send({order_id:order_Id})
    .end((err,response)=>{
      //console.log('>>>'+JSON.stringify(response.text))
      expect(response.status).to.equal(200)
      done();
    })
  });
  before((done)=>{
    agent.post(URL+'/user.signin')
      .send(Distributor_credentials)
      .end(function(err, response){
        expect(response.statusCode).to.equal(200);
        done();
      });
  });
  it("it should let a distributor get the order details",(done)=>{
    agent.get(URL+'/order.details/'+order_Id)
    .end((err,response)=>{
      //console.log(">> "+JSON.stringify(response.text,null,2));
      let data =JSON.parse(response.text);
      //console.log(data._id);
      expect(response.status).to.equal(200);
      expect(data).to.have.property('totalPrice')
      approved_order.order_id=data._id;
      approved_order.amount=data.totalPrice;
      done();
    })
  })
})
describe("Testing order_confirmation",()=>{
  before((done)=>{
    agent.post(URL+"/user.signin")
    .send(Teller_credentials)
    .end((err,response)=>{
      expect(response.statusCode).to.equal(200)
      done()
    })
  })
  it('it should not let a teller confirm an order without an authorization_token',(done)=>{
    newOrder=clone(approved_order);
    newOrder.authorization_token=undefined;
    agent.post(URL+'/order.payment.confirmation')
    .send(newOrder)
    .end(function(err,response){
      expect(response.status).to.equal(400)
      done();
    })
  })
  it('it should not let a teller confirm an order with an invalid authorization_token',(done)=>{
    newOrder=clone(approved_order);
    newOrder.authorization_token="111201-20-193-10-1-39318";
    agent.post(URL+'/order.payment.confirmation')
    .send(newOrder)
    .end(function(err,response){
      expect(response.status).to.equal(400)
      done();
    })
  })
  it('it should not let a teller confirm an order with an empty bankSlip',(done)=>{
    newOrder=clone(approved_order);
    newOrder.bankSlip=undefined;
    agent.post(URL+'/order.payment.confirmation')
    .send(newOrder)
    .end(function(err,response){
      expect(response.status).to.equal(400)
      done();
    })
  })
  it('it should not let a teller confirm an order with an empty order_id',(done)=>{
    newOrder=clone(approved_order);
    newOrder.order_id=undefined;
    agent.post(URL+'/order.payment.confirmation')
    .send(newOrder)
    .end(function(err,response){
      expect(response.status).to.equal(400)
      done();
    })
  })
  it('it should not let a teller confirm an order with an invalid order_id',(done)=>{
    newOrder=clone(approved_order);
    newOrder.order_id="5b30aed182b69651eefv1d7sc";
    agent.post(URL+'/order.payment.confirmation')
    .send(newOrder)
    .end(function(err,response){
      expect(response.status).to.equal(400)
      done();
    })
  })
  it('it should not let a teller confirm an order without the amount',(done)=>{
    newOrder=clone(approved_order);
    newOrder.amount=undefined;
    agent.post(URL+'/order.payment.confirmation')
    .send(newOrder)
    .end(function(err,response){
      expect(response.status).to.equal(400)
      done();
    })
  })
})
describe(">>",()=>{
    before((done)=>{
    agent.post(URL+"/user.signin")
    .send(AD_Credentials)
    .end((err,response)=>{
      expect(response.statusCode).to.equal(200);
      done();
    })
  });
  it("it should not let an agroDealer confirm an order",(done)=>{
    newOrder=clone(approved_order);
      agent.post(URL+'/order.payment.confirmation')
      .send(newOrder)
      .end(function(err,response){
      expect(response.status).to.equal(400)
      done();
      })
  })
  before((done)=>{
    agent.post(URL+"/user.signin")
    .send(Distributor_credentials)
    .end((err,response)=>{
      expect(response.statusCode).to.equal(200);
      done();
    })
  });
  it("it should not let an distributor confirm an order",(done)=>{
    newOrder=clone(approved_order);
      agent.post(URL+'/order.payment.confirmation')
        .send(newOrder)
        .end(function(err,response){
        expect(response.status).to.equal(400)
      done();
      })
  })
  before(function(done){
    agent.post(URL+'/user.signin')
      .send(SUPPLIER_Credentials)
      .end(function(err, response){
      expect(response.statusCode).to.equal(200);
      done();
    });
    });
  it('it should not let a supplier confirm a transaction',(done)=>{
    newOrder=clone(approved_order);
      agent.post(URL+'/order.payment.confirmation')
        .send(newOrder)
        .end(function(err,response){
        expect(response.status).to.equal(400)
        done();
      })
  })
  // before(function(done){
  //   agent.post(URL+'/user.signin')
  //     .send(SA_Credentials)
  //     .end(function(err, response){
  //     expect(response.statusCode).to.equal(200);
  //     done();
  //   });
  //   });
  // it('it should not let a sector_agronomist confirm a transaction',(done)=>{
  //   newOrder=clone(approved_order);
  //     agent.post(URL+'/order.payment.confirmation')
  //       .send(newOrder)
  //       .end(function(err,response){
  //         console.log(response.text);
  //         expect(response.status).to.equal(400)
  //         done();
  //     })
  // })
  it("it should let a teller confirm the payment of an order",function(done){
    agent.post(URL+'/order.payment.confirmation')
    .send(approved_order)
    .end((err,response)=>{
      expect(response.status).to.equal(200);
      done();
    })
  })
  it("it should not let a teller confirm the payment of an order for the second time",function(done){
    agent.post(URL+'/order.payment.confirmation')
    .send(approved_order)
    .end((err,response)=>{
      expect(response.status).to.equal(400);
      done();
    })
  })
})
