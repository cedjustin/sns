let fertilizer_format = {
	"farmer_price":"200",
	"formula":"Fe2(SO4)3",
	"gov_subsidy":"100",
	"name":"Iron III Sulfate",
	"supplier_price":"300",
	"type":"1"
}


//Require the dev-dependencies
let chai = require('chai');
var agent = require('superagent').agent();
let should = chai.should();
let expect = chai.expect;
let assert = chai.assert;
let newOrder;
let newProduct;
let config= require ("./config");

var clone =(a)=>{
return JSON.parse(JSON.stringify(a));
}

let URL =config.URL;

let AD_Credentials ={email:"tunacikone@hvzoi.com",password:"tunacikone@hvzoi.com"},
  SUPPLIER_Credentials ={email:"prucletiki@ququb.com",password:"prucletiki@ququb.com "},
  Distributor_credentials={email:"bilihuchot@2anom.com",password:"bilihuchot@2anom.com"},
  Teller_credentials={email:"beruihuchot@2anome.com",password:"beruihuchot@2anome.com"},
  RAB_credentials={email:"egide.gatari@rab.gov.rw",password:"b88692447d933095a95515c0eb0f7f0382acc36ec4c894057db1379e6b18e51ba"},
  SA_credentials={email:"jeandamantaka@yahoo.com",password:"b88692447d933095a95515c0eb0f7f0382acc36ec4c894057db1379e6b18e51ba"},  
  DistrictA_credentials={email:"tgatoya@gmail.com",password:"b88692447d933095a95515c0eb0f7f0382acc36ec4c894057db1379e6b18e51ba"};

describe("Testing the process to add new fertilizers", ()=>{
	before((done)=>{
		agent.post(URL+"/user.signin")
		.send(RAB_credentials)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		})
	});

	it("It should allow a RAB agent to add new fertilizers", (done)=>{
		agent.post(URL+"/fertilizer.new")
		.send(fertilizer_format)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		})
	})

	it("It should not allow to register the fertilizer twice", (done)=>{
		agent.post(URL+"/fertilizer.new")
		.send(fertilizer_format)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	})

	it("It should not allow a new fertilizer with empty farmer_price", (done)=>{
		let newFertilizer = clone(fertilizer_format);
		newFertilizer.farmer_price = "";
		agent.post(URL+"/fertilizer.new")
		.send(newFertilizer)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	})

	it("It should not allow a new fertilizer with invalid farmer_price", (done)=>{
		let newFertilizer = clone(fertilizer_format);
		newFertilizer.farmer_price = "asl;dfkjk;"
		agent.post(URL+"/fertilizer.new")
		.send(newFertilizer)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	})

	it("It should not allow a new fertilizer with empty formula", (done)=>{
		let newFertilizer = clone(fertilizer_format);
		newFertilizer.formula = "";
		agent.post(URL+"/fertilizer.new")
		.send(newFertilizer)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	})

	it("It should not allow a new fertilizer with invalid formula", (done)=>{
		let newFertilizer = clone(fertilizer_format);
		newFertilizer.formula = "asldkjfk";
		agent.post(URL+"/fertilizer.new")
		.send(newFertilizer)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	})

	it("It should not allow a new fertilizer with empty gov_subsidy", (done)=>{
		let newFertilizer = clone(fertilizer_format);
		newFertilizer.gov_subsidy = "";
		agent.post(URL+"/fertilizer.new")
		.send(newFertilizer)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	})

	it("It should not allow a new fertilizer with invalid gov_subsidy", (done)=>{
		let newFertilizer = clone(fertilizer_format);
		newFertilizer.gov_subsidy = "asdfl;kj";
		agent.post(URL+"/fertilizer.new")
		.send(newFertilizer)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	})

	it("It should not allow a new fertilizer with empty name", (done)=>{
		let newFertilizer = clone(fertilizer_format);
		newFertilizer.name = "";
		agent.post(URL+"/fertilizer.new")
		.send(newFertilizer)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	})

	it("It should not allow a new fertilizer with empty supplier_price", (done)=>{
		let newFertilizer = clone(fertilizer_format);
		newFertilizer.supplier_price = "";
		agent.post(URL+"/fertilizer.new")
		.send(newFertilizer)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	})
	
	it("It should not allow a new fertilizer with invalid supplier_price", (done)=>{
		let newFertilizer = clone(fertilizer_format);
		newFertilizer.supplier_price = "oijasfd";
		agent.post(URL+"/fertilizer.new")
		.send(newFertilizer)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	})

	it("It should not allow a new fertilizer with empty type", (done)=>{
		let newFertilizer = clone(fertilizer_format);
		newFertilizer.type = "";
		agent.post(URL+"/fertilizer.new")
		.send(newFertilizer)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	})

	it("It should not allow a new fertilizer with invalid type", (done)=>{
		let newFertilizer = clone(fertilizer_format);
		newFertilizer.invalid = "oiewuqew";
		agent.post(URL+"/fertilizer.new")
		.send(newFertilizer)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	})

});

describe("Testing whether AD can input fertilizers", ()=>{
	before((done)=>{
		agent.post(URL+"/user.signin")
		.send(AD_Credentials)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		})
	});

	it("It should not let an Agrodealer to add new fertilizers", (done)=>{
		agent.post(URL+"/fertilizer.new")
		.send(fertilizer_format)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	})
})

// describe("Testing whether Supplier can input fertilizers", ()=>{
// 	before((done)=>{
// 		agent.post(URL+"/user.signin")
// 		.send(SUPPLIER_Credentials)
// 		.end((err, response)=>{
// 			expect(response.statusCode).to.equal(200);
// 			done();
// 		})
// 	});

// 	it("It should not allow a Supplier to add new fertilizers", (done)=>{
// 		agent.post(URL+"/fertilizer.new")
// 		.send(fertilizer_format)
// 		.end((err, response)=>{
// 			expect(response.statusCode).to.equal(400);
// 			done();
// 		})
// 	})
// })

describe("Testing whether Teller can input fertilizers", ()=>{
	before((done)=>{
		agent.post(URL+"/user.signin")
		.send(Teller_credentials)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		})
	});

	it("It should not allow a Teller to add new fertilizers", (done)=>{
		agent.post(URL+"/fertilizer.new")
		.send(fertilizer_format)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	})
})

describe("Testing whether Sector Agronomist can input fertilizers", ()=>{
	before((done)=>{
		agent.post(URL+"/user.signin")
		.send(SA_credentials)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		})
	});

	it("It should not allow a Sector Agronomist to add new fertilizers", (done)=>{
		agent.post(URL+"/fertilizer.new")
		.send(fertilizer_format)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	})
})

describe("Testing whether District Agronomist can input fertilizers", ()=>{
	before((done)=>{
		agent.post(URL+"/user.signin")
		.send(DistrictA_credentials)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(200);
			done();
		})
	});

	it("It should not allow a District Agronomist to add new fertilizers", (done)=>{
		agent.post(URL+"/fertilizer.new")
		.send(fertilizer_format)
		.end((err, response)=>{
			expect(response.statusCode).to.equal(400);
			done();
		})
	})
})